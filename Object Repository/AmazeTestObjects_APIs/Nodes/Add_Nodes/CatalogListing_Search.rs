<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Search for a catalog</description>
   <name>CatalogListing_Search</name>
   <tag></tag>
   <elementGuidId>1779497b-57a6-43eb-a9e6-2a5394716b46</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;catalogSearchBar&quot;]/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//*[@id=&quot;catalogSearchBar&quot;]/input</value>
   </webElementXpaths>
</WebElementEntity>
