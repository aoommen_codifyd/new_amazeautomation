<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Add Bulk Root Nodes</name>
   <tag></tag>
   <elementGuidId>53428b71-faa0-4fc4-95f2-85f151e69f17</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;nodeName\&quot;: \&quot;${rootNode_Name}\&quot;},{\&quot;nodeName\&quot;: \&quot;${rootNode_Name2}\&quot;, \&quot;parentId\&quot;:\&quot;\&quot; }]&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>dev001</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>d3vm0d3</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>CODIFYD</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://test.codifyd.com/amazeRest-test/rest/v1/catalogs/2/nodes?domain=2&amp;nodePlacement=F</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.catalog_Id</defaultValue>
      <description></description>
      <id>168fa1c2-c4bc-4f3b-82c6-56180f192a23</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.rootNode_Name</defaultValue>
      <description></description>
      <id>0946d223-4641-4efb-823d-6be69895f458</id>
      <masked>false</masked>
      <name>rootNode_Name</name>
   </variables>
   <variables>
      <defaultValue>'My node'</defaultValue>
      <description></description>
      <id>67249021-2b2a-489d-9bb3-b7782ab1fd00</id>
      <masked>false</masked>
      <name>rootNode_Name2</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()




WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)


</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
