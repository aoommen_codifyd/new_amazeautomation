<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Edit Attribute</name>
   <tag></tag>
   <elementGuidId>68bcb894-709a-45e2-bc3e-5d99b9f2ad67</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;id\&quot;:\&quot;${attribute_Id}\&quot;,\&quot;catalog\&quot;:{\&quot;id\&quot;:67},\&quot;customerAttributeId\&quot;:\&quot;\&quot;,\&quot;name\&quot;:\&quot;${edit_Name}\&quot;,\&quot;dataType\&quot;:\&quot;${edit_DataType}\&quot;,\&quot;global\&quot;:\&quot;${edit_isGlobal}\&quot;,\&quot;canHaveLov\&quot;:true,\&quot;canHaveMultipleUom\&quot;:true,\&quot;uoms\&quot;:[{\&quot;id\&quot;:83,\&quot;name\&quot;:\&quot;Pascal\&quot;,\&quot;uomObj\&quot;:{\&quot;id\&quot;:83,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;userFirstName\&quot;:\&quot;Amaze\&quot;,\&quot;userLastName\&quot;:\&quot;SuperUser\&quot;,\&quot;userEmailId\&quot;:\&quot;amaze_superuser@codifyd.com\&quot;,\&quot;userPhoneNumber\&quot;:\&quot;312-243-1140\&quot;,\&quot;userAddress1\&quot;:\&quot;303 E Wacker Ave\&quot;,\&quot;userAddress2\&quot;:\&quot;Suite 950\&quot;,\&quot;zipCode\&quot;:\&quot;60061\&quot;,\&quot;userAccessStartDate\&quot;:\&quot;2019-06-25T00:00:00.000+0000\&quot;,\&quot;userAccessEndDate\&quot;:\&quot;2099-12-31T00:00:00.000+0000\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:true},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;userFirstName\&quot;:\&quot;Amaze\&quot;,\&quot;userLastName\&quot;:\&quot;SuperUser\&quot;,\&quot;userEmailId\&quot;:\&quot;amaze_superuser@codifyd.com\&quot;,\&quot;userPhoneNumber\&quot;:\&quot;312-243-1140\&quot;,\&quot;userAddress1\&quot;:\&quot;303 E Wacker Ave\&quot;,\&quot;userAddress2\&quot;:\&quot;Suite 950\&quot;,\&quot;zipCode\&quot;:\&quot;60061\&quot;,\&quot;userAccessStartDate\&quot;:\&quot;2019-06-25T00:00:00.000+0000\&quot;,\&quot;userAccessEndDate\&quot;:\&quot;2099-12-31T00:00:00.000+0000\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:true},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;name\&quot;:\&quot;Pascal\&quot;,\&quot;symbol\&quot;:\&quot;Pa\&quot;,\&quot;category\&quot;:\&quot;Pressure\&quot;}}],\&quot;lovs\&quot;:[{\&quot;value\&quot;:\&quot;${edit_Lov}\&quot;,\&quot;uom\&quot;:{\&quot;id\&quot;:83,\&quot;name\&quot;:\&quot;Pascal\&quot;},\&quot;displayOrder\&quot;:0}]}]&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>dev001</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>d3vm0d3</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>CODIFYD</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>http://test.codifyd.com/amazeRest-test/rest/v1/catalogs/${catalog_Id}/attributes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.attribute_Id</defaultValue>
      <description></description>
      <id>3430d31b-87bb-44a8-bc5a-d2bb8bf74902</id>
      <masked>false</masked>
      <name>attribute_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.catalog_Id</defaultValue>
      <description></description>
      <id>105c4828-5a1c-47ea-8d63-3f3206c7d7c1</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.edit_Name</defaultValue>
      <description></description>
      <id>9f3625c7-ed54-4e28-a112-2f2527c483f6</id>
      <masked>false</masked>
      <name>edit_Name</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.edit_DataType</defaultValue>
      <description></description>
      <id>1313909a-5b88-456f-9b50-39bc83fa4f6d</id>
      <masked>false</masked>
      <name>edit_DataType</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.edit_Lov</defaultValue>
      <description></description>
      <id>74a43eb6-6dc6-48e7-a2fb-627c203af213</id>
      <masked>false</masked>
      <name>edit_Lov</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.edit_isGlobal</defaultValue>
      <description></description>
      <id>cf4ec9ca-1ba9-4de9-a96a-ea9c4ab79673</id>
      <masked>false</masked>
      <name>edit_isGlobal</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
