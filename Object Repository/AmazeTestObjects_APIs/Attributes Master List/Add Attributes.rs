<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Add Attributes</name>
   <tag></tag>
   <elementGuidId>46e99dea-a268-4169-862f-ee04ab261f75</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;catalog\&quot;:{\&quot;id\&quot;:\&quot;${catalog_Id}\&quot;},\&quot;customerAttributeId\&quot;:\&quot;\&quot;,\&quot;name\&quot;:\&quot;${attr_Name}\&quot;,\&quot;dataType\&quot;:\&quot;${data_Type}\&quot;,\&quot;global\&quot;:false,\&quot;canHaveLov\&quot;:true,\&quot;canHaveMultipleUom\&quot;:true,\&quot;uoms\&quot;:[{\&quot;id\&quot;:83,\&quot;value\&quot;:\&quot;Pascal\&quot;,\&quot;uomObj\&quot;:{\&quot;id\&quot;:83,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;userFirstName\&quot;:\&quot;Amaze\&quot;,\&quot;userLastName\&quot;:\&quot;SuperUser\&quot;,\&quot;userEmailId\&quot;:\&quot;amaze_superuser@codifyd.com\&quot;,\&quot;userPhoneNumber\&quot;:\&quot;312-243-1140\&quot;,\&quot;userAddress1\&quot;:\&quot;303 E Wacker Ave\&quot;,\&quot;userAddress2\&quot;:\&quot;Suite 950\&quot;,\&quot;zipCode\&quot;:\&quot;60061\&quot;,\&quot;userAccessStartDate\&quot;:\&quot;2019-06-25T00:00:00.000+0000\&quot;,\&quot;userAccessEndDate\&quot;:\&quot;2099-12-31T00:00:00.000+0000\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:true},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;userFirstName\&quot;:\&quot;Amaze\&quot;,\&quot;userLastName\&quot;:\&quot;SuperUser\&quot;,\&quot;userEmailId\&quot;:\&quot;amaze_superuser@codifyd.com\&quot;,\&quot;userPhoneNumber\&quot;:\&quot;312-243-1140\&quot;,\&quot;userAddress1\&quot;:\&quot;303 E Wacker Ave\&quot;,\&quot;userAddress2\&quot;:\&quot;Suite 950\&quot;,\&quot;zipCode\&quot;:\&quot;60061\&quot;,\&quot;userAccessStartDate\&quot;:\&quot;2019-06-25T00:00:00.000+0000\&quot;,\&quot;userAccessEndDate\&quot;:\&quot;2099-12-31T00:00:00.000+0000\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:true},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;name\&quot;:\&quot;Pascal\&quot;,\&quot;symbol\&quot;:\&quot;Pa\&quot;,\&quot;category\&quot;:\&quot;Pressure\&quot;}}],\&quot;lovs\&quot;:[{\&quot;value\&quot;:\&quot;87.9\&quot;,\&quot;uom\&quot;:{\&quot;id\&quot;:83},\&quot;displayOrder\&quot;:0}]}]&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>dev001</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic YW9vbW1lbkBjb2RpZnlkLmNvbTpwYXNzd29yZA==</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>d3vm0d3</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>CODIFYD</value>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/rest/v1/catalogs/${catalog_Id}/attributes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.catalogID</defaultValue>
      <description></description>
      <id>340d0f74-1042-4cdc-9f24-0170ba11e6a8</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.attr_Name</defaultValue>
      <description></description>
      <id>9622c81d-e069-4281-ad0d-e1f88cfbef3b</id>
      <masked>false</masked>
      <name>attr_Name</name>
   </variables>
   <variables>
      <defaultValue>'STRING'</defaultValue>
      <description></description>
      <id>95aba50c-7dcd-41fc-883b-81680fcae70a</id>
      <masked>false</masked>
      <name>data_Type</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.editAttrAPI</defaultValue>
      <description></description>
      <id>a2acc05f-29f1-4c96-8ff7-6e3dba436bf2</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
