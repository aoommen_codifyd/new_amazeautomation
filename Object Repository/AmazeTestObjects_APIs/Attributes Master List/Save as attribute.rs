<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Save as attribute</name>
   <tag></tag>
   <elementGuidId>b436a121-308e-4202-8e80-830abe5304f4</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;catalog\&quot;:{\&quot;id\&quot;:\&quot;${catalog_Id}\&quot;},\&quot;customerAttributeId\&quot;:\&quot;\&quot;,\&quot;name\&quot;:\&quot;${saveAs_Name}\&quot;,\&quot;dataType\&quot;:\&quot;${saveAs_DataType}\&quot;,\&quot;global\&quot;:false,\&quot;canHaveLov\&quot;:true,\&quot;canHaveMultipleUom\&quot;:true,\&quot;uoms\&quot;:[{\&quot;id\&quot;:83,\&quot;value\&quot;:\&quot;Pascal\&quot;,\&quot;uomObj\&quot;:{\&quot;id\&quot;:83,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;userFirstName\&quot;:\&quot;Amaze\&quot;,\&quot;userLastName\&quot;:\&quot;SuperUser\&quot;,\&quot;userEmailId\&quot;:\&quot;amaze_superuser@codifyd.com\&quot;,\&quot;userPhoneNumber\&quot;:\&quot;312-243-1140\&quot;,\&quot;userAddress1\&quot;:\&quot;303 E Wacker Ave\&quot;,\&quot;userAddress2\&quot;:\&quot;Suite 950\&quot;,\&quot;zipCode\&quot;:\&quot;60061\&quot;,\&quot;userAccessStartDate\&quot;:\&quot;2019-06-25T00:00:00.000+0000\&quot;,\&quot;userAccessEndDate\&quot;:\&quot;2099-12-31T00:00:00.000+0000\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:true},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;userFirstName\&quot;:\&quot;Amaze\&quot;,\&quot;userLastName\&quot;:\&quot;SuperUser\&quot;,\&quot;userEmailId\&quot;:\&quot;amaze_superuser@codifyd.com\&quot;,\&quot;userPhoneNumber\&quot;:\&quot;312-243-1140\&quot;,\&quot;userAddress1\&quot;:\&quot;303 E Wacker Ave\&quot;,\&quot;userAddress2\&quot;:\&quot;Suite 950\&quot;,\&quot;zipCode\&quot;:\&quot;60061\&quot;,\&quot;userAccessStartDate\&quot;:\&quot;2019-06-25T00:00:00.000+0000\&quot;,\&quot;userAccessEndDate\&quot;:\&quot;2099-12-31T00:00:00.000+0000\&quot;,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:true},\&quot;updatedDate\&quot;:\&quot;2019-06-25T18:33:58.000+0000\&quot;,\&quot;name\&quot;:\&quot;Pascal\&quot;,\&quot;symbol\&quot;:\&quot;Pa\&quot;,\&quot;category\&quot;:\&quot;Pressure\&quot;}}],\&quot;lovs\&quot;:[{\&quot;value\&quot;:\&quot;${saveAs_Lov}\&quot;,\&quot;uom\&quot;:{\&quot;id\&quot;:83},\&quot;displayOrder\&quot;:0}]}]&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>dev001</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>d3vm0d3</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>CODIFYD</value>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://test.codifyd.com/amazeRest-test/rest/v1/catalogs/${catalog_Id}/attributes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.catalog_Id</defaultValue>
      <description></description>
      <id>5222f791-5428-4669-8051-129e85841c3b</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.saveAs_Name</defaultValue>
      <description></description>
      <id>43035886-c85c-422e-a12b-b80a49251172</id>
      <masked>false</masked>
      <name>saveAs_Name</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.edit_DataType</defaultValue>
      <description></description>
      <id>ffdd45a8-01ff-44c4-bd28-fa80198a138a</id>
      <masked>false</masked>
      <name>saveAs_DataType</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.saveAs_Lov</defaultValue>
      <description></description>
      <id>e1015afe-4201-4706-97ab-1c933dc163ad</id>
      <masked>false</masked>
      <name>saveAs_Lov</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
