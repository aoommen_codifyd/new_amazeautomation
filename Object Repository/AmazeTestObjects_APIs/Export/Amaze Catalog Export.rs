<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Amaze Catalog Export</name>
   <tag></tag>
   <elementGuidId>43da1e66-d040-455f-a6ac-051859a11742</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\&quot;exportName\&quot;:\&quot;${exportName}\&quot;,\n\&quot;catalog\&quot;:{\&quot;id\&quot;:${catalog_Id}},\n\&quot;catalogDomain\&quot;:{\&quot;id\&quot;:\&quot;60\&quot;},\n\&quot;exportAttributes\&quot;:true,\n\&quot;exportSchemas\&quot;:true,\n\&quot;exportSkus\&quot;:true,\n\&quot;exportTaxonomies\&quot;:true,\n\&quot;exportDigitalAssets\&quot;:true,\n\&quot;exportType\&quot;:\&quot;${fileType}\&quot;,\n\&quot;exportOption\&quot;:\&quot;SELECTED_NODES\&quot;,\n\&quot;taxonomyIds\&quot;:[\&quot;1872\&quot;],\n\&quot;fieldDelimiter\&quot;:\&quot;\\\\t\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>f42da7ef-2f89-4dc3-ab21-cc6f7e26d895</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>dev001</value>
      <webElementGuid>4385d5bd-fb9a-4691-8056-c01bba195206</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>rEr8f4UFNgRnO1Z5tnvjtxiyz9flnoGYP9b1rf+URh2sfafCRzJfskJt</value>
      <webElementGuid>6dfa1625-7ebe-4768-a131-d272e11c7f22</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>content-language</name>
      <type>Main</type>
      <value>en-US</value>
      <webElementGuid>50c1a4a6-deba-48e2-ad6e-1c84b4698f7b</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Client-Secret</name>
      <type>Main</type>
      <value>allwyn</value>
      <webElementGuid>f933a4de-65cc-4677-a217-a9b1a106a67b</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>7.8.2</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://apis-test.bluemeteor.com/amazeApiRest-test/rest/v1/exports</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.catalog_Id</defaultValue>
      <description></description>
      <id>79d76128-a790-43e3-b796-f205ef5bb40d</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>'AmazeExport_'</defaultValue>
      <description></description>
      <id>46b720bd-16f2-4261-a5a8-afcd67403856</id>
      <masked>false</masked>
      <name>exportName</name>
   </variables>
   <variables>
      <defaultValue>'Standard Horizontal Delimited'</defaultValue>
      <description></description>
      <id>4e7bd9e4-dfe4-4963-986c-0105afc4b194</id>
      <masked>false</masked>
      <name>fileType</name>
   </variables>
   <variables>
      <defaultValue>'15259'</defaultValue>
      <description></description>
      <id>df9067fa-3256-4dbe-adbf-fb90416a8ca6</id>
      <masked>false</masked>
      <name>nodeId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
