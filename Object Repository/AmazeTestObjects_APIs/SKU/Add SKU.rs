<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Add SKU</name>
   <tag></tag>
   <elementGuidId>a3e8ddde-0619-49b2-91d7-837e51a3cc76</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;catalog\&quot;:{\&quot;id\&quot;:23},\&quot;customerSkuId\&quot;:\&quot;3456\&quot;,\&quot;description\&quot;:\&quot;test\&quot;,\&quot;type\&quot;:\&quot;SKU\&quot;,\&quot;skuAttributes\&quot;:[{\&quot;attribute\&quot;:{\&quot;id\&quot;:60075},\&quot;attributeValues\&quot;:[{\&quot;value\&quot;:\&quot;tesyt\&quot;}]}]}]&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>dev001</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>ya29.a0AfH6SMBtRpCenLeaIJNxFTLnX7FCnU8y76dt84nVLfuygao8PB2OFemyswrrrBa_x9Gnmyb94l_XkBNrPXWYVSPkFLiUxM367KNioC4X8JkBOL44qufpCbtvJhjNnTDGGqmC8alnxJcQ9dUJ37h8HjqnUlw-</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>google</value>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://amazeapistest.bluemeteor.com/amazeRest-test/rest/v1/catalogs/23/nodes/22667/skus?catalogDomainId=60&amp;_=1616742507550</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'23'</defaultValue>
      <description></description>
      <id>eaf1df7c-a73e-42e6-959c-f78a37349665</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.sku_Id</defaultValue>
      <description></description>
      <id>5ead7f4e-12b4-4f1d-808c-f2279d1dcdc1</id>
      <masked>false</masked>
      <name>sku_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.attribute_Id</defaultValue>
      <description></description>
      <id>4bc29b2d-d9e7-43c0-8038-3a4281f8fff9</id>
      <masked>false</masked>
      <name>attribute_Id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
