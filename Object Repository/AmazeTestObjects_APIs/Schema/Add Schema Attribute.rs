<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Add Schema Attribute</name>
   <tag></tag>
   <elementGuidId>23e6f1d2-28f4-49ae-9624-b485462d7662</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;attributeId\&quot;:${attribute_Id},\&quot;inSchema\&quot;:true,\&quot;schemaConstraints\&quot;:[{\&quot;constraintName\&quot;:\&quot;DISALLOW_MULTIVALUES\&quot;}],\&quot;uoms\&quot;:[],\&quot;lovs\&quot;:[]}]&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>allwyn</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>d3vm0d3</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>CODIFYD</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://test.codifyd.com/amazeRest-test/rest/v1/catalogs/2/nodes/197/schemaAttributes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.catalog_Id</defaultValue>
      <description></description>
      <id>2a2d1c57-a754-4e88-84cb-911795bca243</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.attribute_Id</defaultValue>
      <description></description>
      <id>b6ee560f-f8df-428a-a9c7-e26fe6b3c317</id>
      <masked>false</masked>
      <name>attribute_Id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
