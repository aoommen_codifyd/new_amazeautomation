<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Edit Schema</name>
   <tag></tag>
   <elementGuidId>526fc3ae-9fa3-4810-b222-a69c6d24e7c1</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;id\&quot;:${schema_Id},\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-10-03T12:07:38.000+0000\&quot;,\&quot;catalogDomain\&quot;:{\&quot;id\&quot;:72,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-07-25T12:05:55.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-07-25T12:05:55.000+0000\&quot;,\&quot;catalog\&quot;:{\&quot;id\&quot;:67,\&quot;customerCode\&quot;:\&quot;dev001\&quot;,\&quot;createdBy\&quot;:{\&quot;id\&quot;:1,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;createdDate\&quot;:\&quot;2019-07-25T12:05:55.000+0000\&quot;,\&quot;updatedBy\&quot;:{\&quot;id\&quot;:1,\&quot;active\&quot;:true,\&quot;systemUser\&quot;:false},\&quot;updatedDate\&quot;:\&quot;2019-07-25T12:05:55.000+0000\&quot;,\&quot;catalogName\&quot;:\&quot;TestCatalog_Allwyn\&quot;,\&quot;catalogDescription\&quot;:\&quot;test\&quot;},\&quot;domainDescription\&quot;:\&quot;Primary Taxonomy Hierarchy\&quot;,\&quot;domainName\&quot;:\&quot;Primary\&quot;,\&quot;catalogId\&quot;:67,\&quot;primary\&quot;:true},\&quot;attributeDisplayOrder\&quot;:2,\&quot;userComments\&quot;:\&quot;Automated system created schema for global attribute\&quot;,\&quot;catalogId\&quot;:67,\&quot;attributeId\&quot;:43536,\&quot;inSchema\&quot;:true,\&quot;schemaConstraints\&quot;:[{\&quot;constraintName\&quot;:\&quot;ENFORCE_LOV\&quot;},{\&quot;constraintName\&quot;:\&quot;ENFORCE_UOM\&quot;}],\&quot;uoms\&quot;:[{\&quot;id\&quot;:83,\&quot;value\&quot;:\&quot;Pascal\&quot;}],\&quot;lovs\&quot;:[{\&quot;value\&quot;:\&quot;87.9\&quot;,\&quot;uom\&quot;:{\&quot;id\&quot;:83,\&quot;name\&quot;:\&quot;Pascal\&quot;},\&quot;displayOrder\&quot;:0}]}]&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>allwyn</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>d3vm0d3</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>CODIFYD</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>https://test.codifyd.com/amazeRest-test/rest/v1/catalogs/2/nodes/197/schemaAttributes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.catalog_Id</defaultValue>
      <description></description>
      <id>14824045-3e80-42ee-b136-4db58faa193c</id>
      <masked>false</masked>
      <name>catalog_Id</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.schema_Id</defaultValue>
      <description></description>
      <id>1965252f-43f8-41c6-927b-461934b0768d</id>
      <masked>false</masked>
      <name>schema_Id</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
