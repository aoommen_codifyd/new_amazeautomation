<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>Add UOM API test</description>
   <name>Add UOM</name>
   <tag></tag>
   <elementGuidId>054045ae-3707-4a4b-bbda-19a630995478</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;[{\&quot;name\&quot;:\&quot;${UOM_Name}\&quot;,\&quot;symbol\&quot;:\&quot;${UOM_Symbol}\&quot;,\&quot;category\&quot;:\&quot;${UOM_Category}\&quot;}]&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Customer-Code</name>
      <type>Main</type>
      <value>allwyn</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Access-Token</name>
      <type>Main</type>
      <value>d3vm0d3</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>X-Oauth-Provider</name>
      <type>Main</type>
      <value>CODIFYD</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://test.codifyd.com/amazeRest-test/rest/v1/uoms</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.UOM_Name</defaultValue>
      <description></description>
      <id>e62192ba-f205-4376-a17f-3f9860fa9340</id>
      <masked>false</masked>
      <name>UOM_Name</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.UOM_Symbol</defaultValue>
      <description></description>
      <id>5d42406e-f9a6-4c9e-b34d-4a1539a4f9f6</id>
      <masked>false</masked>
      <name>UOM_Symbol</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.UOM_Category</defaultValue>
      <description></description>
      <id>f3a71500-b90d-4a27-a870-3d30d7f5f7df</id>
      <masked>false</masked>
      <name>UOM_Category</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
