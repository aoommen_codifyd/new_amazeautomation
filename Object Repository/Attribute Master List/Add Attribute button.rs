<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add Attribute button</name>
   <tag></tag>
   <elementGuidId>bfe87396-4440-435a-b7d4-8b854c7c141d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@mattooltip='Add Attribute(s)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
</WebElementEntity>
