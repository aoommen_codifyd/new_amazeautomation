<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Submit PF button</name>
   <tag></tag>
   <elementGuidId>c3d13ac2-052a-45c9-8f4d-fda0fcf0a668</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id,&quot;mat-dialog&quot;)]/amaze-dialog/mat-dialog-content/add-productfamily-dialog/div/div/form/div[2]/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
