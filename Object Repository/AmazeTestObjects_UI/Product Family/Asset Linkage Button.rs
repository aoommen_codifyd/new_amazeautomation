<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Asset Linkage Button</name>
   <tag></tag>
   <elementGuidId>80ae0f12-208f-4529-a325-63b3a1996b19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;proFam-toolbar&quot;]/div[5]/div/button/span/span[contains(text(), 'Digital Assets')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
