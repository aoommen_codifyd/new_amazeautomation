<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>button</description>
   <name>Add Child Node</name>
   <tag></tag>
   <elementGuidId>d36d82fa-ed44-4a1a-9ffe-33bb6fdd3e90</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[text()='Add Child PF']/parent::button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
