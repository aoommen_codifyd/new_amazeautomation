<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Google password</description>
   <name>google_password</name>
   <tag></tag>
   <elementGuidId>42b6e40e-0d7d-47cb-905e-b78513c201ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name=&quot;password&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
