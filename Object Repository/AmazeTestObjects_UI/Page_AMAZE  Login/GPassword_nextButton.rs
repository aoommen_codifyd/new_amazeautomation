<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Google next button after entering the pasword</description>
   <name>GPassword_nextButton</name>
   <tag></tag>
   <elementGuidId>b223411a-995f-4209-b425-70bbbb6d9a7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;passwordNext&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
