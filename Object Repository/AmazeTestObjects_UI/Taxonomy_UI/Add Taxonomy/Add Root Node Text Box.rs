<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Add root node pop up</description>
   <name>Add Root Node Text Box</name>
   <tag></tag>
   <elementGuidId>6e24360e-c1db-4021-86da-22ac056a8dbb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@placeholder = &quot;Type Node Name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
