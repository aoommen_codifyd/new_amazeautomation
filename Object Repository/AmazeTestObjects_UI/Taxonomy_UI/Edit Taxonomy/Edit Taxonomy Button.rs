<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>//button[@mattooltip=&quot;Edit Node(s)&quot;]</description>
   <name>Edit Taxonomy Button</name>
   <tag></tag>
   <elementGuidId>4c42d009-ee7b-4dfb-9d33-a495d65be62a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@mattooltip=&quot;Edit Node(s)&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
