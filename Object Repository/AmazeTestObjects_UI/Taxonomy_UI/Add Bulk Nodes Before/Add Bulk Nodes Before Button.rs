<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add Bulk Nodes Before Button</name>
   <tag></tag>
   <elementGuidId>60edde2c-9249-420b-97ff-c7f21585e994</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space(text())=&quot;Add Multiple Nodes Before&quot;]/parent::button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
