<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add More Nodes 1</name>
   <tag></tag>
   <elementGuidId>bbaf90d8-87e9-483b-8d15-9cd0b842f601</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id,&quot;cdk-drop-list-&quot;)]//button[@mattooltip=&quot;Add More&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
