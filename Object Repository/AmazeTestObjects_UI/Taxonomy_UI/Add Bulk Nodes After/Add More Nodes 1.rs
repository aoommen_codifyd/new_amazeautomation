<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add More Nodes 1</name>
   <tag></tag>
   <elementGuidId>17380113-c267-4f6d-93fd-a4712c2d9508</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id,&quot;cdk-drop-list-&quot;)]//button[@mattooltip=&quot;Add More&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
