<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Asset Name Search box</name>
   <tag></tag>
   <elementGuidId>6fb3e927-8cdb-472f-b3ad-dd66184b78f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//small[text()=&quot;Asset Name&quot;]/following-sibling::input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
