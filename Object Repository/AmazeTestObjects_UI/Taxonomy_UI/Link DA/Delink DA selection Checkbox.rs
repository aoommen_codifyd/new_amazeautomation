<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delink DA selection Checkbox</name>
   <tag></tag>
   <elementGuidId>6d6fa8a1-2cba-492b-bc5c-f9dff5a9eb91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//mat-checkbox[contains(@id,&quot;mat-checkbox&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
