<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delete Node Link</name>
   <tag></tag>
   <elementGuidId>fb4cd059-e2ec-484b-aaa5-b082cfaf85e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[text()=&quot;delete&quot;]/parent::span/parent::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
