<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Search Node for Deletion</name>
   <tag></tag>
   <elementGuidId>770da0b0-36ef-4b68-a882-1f8fdbf292f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//amaze-tree-search[@ng-reflect-from-popup='true']//input[@id='searchCriteriaField']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
