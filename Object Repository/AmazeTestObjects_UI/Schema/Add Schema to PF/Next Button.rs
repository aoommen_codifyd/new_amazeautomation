<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Next Button</name>
   <tag></tag>
   <elementGuidId>f5a8beae-e13e-4b78-b878-32f10852bd5c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[3]/div[2]/div/mat-dialog-container/amaze-dialog/mat-dialog-content/app-add-remove-attribute-productfamily-popup/div/mat-horizontal-stepper/div[2]/div[1]/div[3]/div/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
