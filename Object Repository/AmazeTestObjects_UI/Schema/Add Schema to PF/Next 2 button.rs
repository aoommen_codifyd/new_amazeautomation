<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Next 2 button</name>
   <tag></tag>
   <elementGuidId>f4f9f605-6703-4eea-988e-ab59a5838667</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[3]/div[2]/div/mat-dialog-container/amaze-dialog/mat-dialog-content/app-add-remove-attribute-productfamily-popup/div/mat-horizontal-stepper/div[2]/div[2]/div/div/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
