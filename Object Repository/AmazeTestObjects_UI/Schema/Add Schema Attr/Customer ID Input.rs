<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Customer ID Input</name>
   <tag></tag>
   <elementGuidId>9d6bdb84-32f0-4d70-9e10-a1d522350f66</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;schemaEntryWrap0&quot;]/div[1]/table/tr[1]/td[2]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
