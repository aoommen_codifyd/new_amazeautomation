<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Confirm Schema Submit</name>
   <tag></tag>
   <elementGuidId>de9574f6-ff3f-4b89-99b8-35b121ab8312</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id,&quot;mat-dialog&quot;)]/amaze-dialog/mat-dialog-content/delete-schema-dialog/div/div[2]/div/button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
