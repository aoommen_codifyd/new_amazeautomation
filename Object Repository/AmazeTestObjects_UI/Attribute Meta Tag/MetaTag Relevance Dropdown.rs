<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MetaTag Relevance Dropdown</name>
   <tag></tag>
   <elementGuidId>382dcd84-0774-441c-9a1e-ce8aca2cd129</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[contains(@formcontrolname,&quot;metaRelevance&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
