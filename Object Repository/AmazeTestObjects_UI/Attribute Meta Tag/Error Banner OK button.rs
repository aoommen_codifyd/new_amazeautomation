<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>While adding same name meta tag</description>
   <name>Error Banner OK button</name>
   <tag></tag>
   <elementGuidId>456422cc-002a-4fa9-a711-0b280b931cae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(), 'OK')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
