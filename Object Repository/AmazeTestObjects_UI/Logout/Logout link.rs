<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Logout link</name>
   <tag></tag>
   <elementGuidId>91dd4378-4533-4013-94cb-82220beae817</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//mat-toolbar[@id='site-header']/mat-toolbar-row/div/div/div/span[contains(text(), 'Hi,')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
