<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Delete &quot;Yes&quot; button</description>
   <name>SKU Delete Confirmation</name>
   <tag></tag>
   <elementGuidId>3b7c5142-d66f-450f-bfff-e4a4df860e8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@ng-reflect-type=&quot;submit&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
