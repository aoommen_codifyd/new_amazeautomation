<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add SKU button</name>
   <tag></tag>
   <elementGuidId>7ac70211-d12d-4729-942a-03dfaf49ce6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), &quot;Cancel&quot;)]/parent::button/preceding-sibling::button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
