<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Edit SKUs button on the dropdown list</description>
   <name>Edit SKUs option</name>
   <tag></tag>
   <elementGuidId>f721daff-fda3-43f0-a96d-ec0a303fc9b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'Edit SKU(s)')]/parent::button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
