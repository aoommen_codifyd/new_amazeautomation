<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Attribute Value Add Button</name>
   <tag></tag>
   <elementGuidId>c01b984e-e48d-4013-87e4-a1db6f314c6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id,&quot;cdk-overlay-&quot;)]/div/div[3]/div/div/div[3]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
