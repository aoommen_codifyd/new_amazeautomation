<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Link DA Submit Button</name>
   <tag></tag>
   <elementGuidId>47e20487-7276-47d5-ad2e-be9e789e57b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space(text())=&quot;Submit&quot;]/parent::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
