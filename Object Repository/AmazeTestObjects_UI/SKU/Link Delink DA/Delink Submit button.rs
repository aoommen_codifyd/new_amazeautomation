<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delink Submit button</name>
   <tag></tag>
   <elementGuidId>eaec5f4d-cf4b-4e45-886e-eb790dc2c405</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space(text())=&quot;Submit&quot;]/parent::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
