<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select All Checkbox Delink</name>
   <tag></tag>
   <elementGuidId>89845b3c-097d-4ea4-a807-781b2e0a9091</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//th[contains(text(), &quot;Asset&quot;)]/preceding-sibling::th/mat-checkbox/label/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
