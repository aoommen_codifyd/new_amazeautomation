<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Filter Input textbox</name>
   <tag></tag>
   <elementGuidId>431f7b7c-d46c-41e1-b5ad-0e8a6b7f5566</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(),&quot;SKU Item ID: &quot;)]/following-sibling::input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
