<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SKU ID Filter Checkbox</name>
   <tag></tag>
   <elementGuidId>fcde9d35-5e16-4e1a-be7c-951af3090bae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[normalize-space(text())=&quot;SKU Item ID&quot;]/preceding-sibling::mat-pseudo-checkbox</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
