<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>View SKU crosslisting</description>
   <name>View Crosslisting Button</name>
   <tag></tag>
   <elementGuidId>be5113dd-2a83-43c9-86a9-ec9125ebff1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'View Cross Listed Information')]/parent::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
