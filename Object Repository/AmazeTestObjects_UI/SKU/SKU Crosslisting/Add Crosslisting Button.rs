<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Add SKU cross listing</description>
   <name>Add Crosslisting Button</name>
   <tag></tag>
   <elementGuidId>bc34279f-8764-42c1-b83f-65c7e2729337</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'Add SKU Cross Listing')]/parent::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
