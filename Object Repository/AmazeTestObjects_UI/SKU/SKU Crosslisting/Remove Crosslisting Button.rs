<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Remove SKU crosslisting</description>
   <name>Remove Crosslisting Button</name>
   <tag></tag>
   <elementGuidId>f49a8dc6-fa66-47b8-a911-d2f495d97dfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'Remove SKU Cross Listing')]/parent::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
