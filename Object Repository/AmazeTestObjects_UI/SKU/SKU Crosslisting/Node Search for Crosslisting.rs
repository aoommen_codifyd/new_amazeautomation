<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Node Search for Crosslisting</name>
   <tag></tag>
   <elementGuidId>5f7387b3-fd4c-4793-94a0-f3a6df19a44d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//add-crosslist-sku-dialog//input[@id='searchCriteriaField']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
