<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Taxonomy search box on the move SKU popup.</description>
   <name>Taxonomy Search Box</name>
   <tag></tag>
   <elementGuidId>596f04cb-f59d-4d61-908f-52c5fd2c6485</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//amaze-tree-search[@ng-reflect-tree=&quot;moveSKUsToNode-tree&quot;]/div/div/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
