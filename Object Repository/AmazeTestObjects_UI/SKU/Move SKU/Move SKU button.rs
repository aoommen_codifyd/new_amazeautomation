<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Move SKU button object on the SKU panel</description>
   <name>Move SKU button</name>
   <tag></tag>
   <elementGuidId>991a184d-b74d-4842-b981-1c06e1655145</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@mattooltip=&quot;Move SKUs&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
