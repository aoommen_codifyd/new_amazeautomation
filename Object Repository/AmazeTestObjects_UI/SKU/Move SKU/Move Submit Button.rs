<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Move Submit Button</name>
   <tag></tag>
   <elementGuidId>941ce891-2e36-48b8-8a06-8809c64909c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//move-sku-dialog/div/button/span[contains(text(), 'Move')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
