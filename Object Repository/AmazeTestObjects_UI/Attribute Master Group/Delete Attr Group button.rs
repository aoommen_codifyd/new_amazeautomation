<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delete Attr Group button</name>
   <tag></tag>
   <elementGuidId>860e34bf-a87c-47a6-86a3-7d7f6ff5d69f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(@mattooltip,&quot;Delete Attribute Group&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
