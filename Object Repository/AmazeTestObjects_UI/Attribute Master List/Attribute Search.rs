<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Attribute name search box</description>
   <name>Attribute Search</name>
   <tag></tag>
   <elementGuidId>e0c6686e-3aa7-4e00-888e-38bab893eff9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=&quot;gs_name&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
