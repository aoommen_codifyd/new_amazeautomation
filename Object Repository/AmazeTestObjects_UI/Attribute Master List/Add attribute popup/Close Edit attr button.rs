<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Close Edit attr button</name>
   <tag></tag>
   <elementGuidId>6bc6d3d3-bf0d-47a7-aea1-f7f1d4be6838</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;mat-dialog-0&quot;]/amaze-dialog/mat-dialog-content/edit-attribute-dialog/div/div/form/div[2]/button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
