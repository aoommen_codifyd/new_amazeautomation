<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Attribute Present</name>
   <tag></tag>
   <elementGuidId>4f4f46e8-6bb0-4b35-baf7-7735ecff71a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//small[contains(@title,&quot;already exists&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
