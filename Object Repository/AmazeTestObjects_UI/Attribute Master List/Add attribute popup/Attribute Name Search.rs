<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Attribute Name Search box</description>
   <name>Attribute Name Search</name>
   <tag></tag>
   <elementGuidId>23e55db6-8395-4386-b76c-5cf2804dfba2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;gs_name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
