<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DateTime Set Button</name>
   <tag></tag>
   <elementGuidId>726a3efb-2111-4914-8a56-a2c2148ba15c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[text()=&quot;Set&quot;]/parent::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
