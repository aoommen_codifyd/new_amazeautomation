<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LoV Toggle Button 2</name>
   <tag></tag>
   <elementGuidId>ec6daa26-a9a4-48ae-b195-f6deb5f5aec5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@ng-reflect-name=&quot;1&quot;]//input[@ng-reflect-name=&quot;canHaveLovs&quot;]/following-sibling::span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
