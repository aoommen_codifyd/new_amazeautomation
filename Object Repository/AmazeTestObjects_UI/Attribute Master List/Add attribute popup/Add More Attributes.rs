<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add More Attributes</name>
   <tag></tag>
   <elementGuidId>4ada027f-f208-4b89-b935-4a971204e3f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//mat-icon[text()=&quot;add&quot;]/parent::span/parent::button[@mattooltip=&quot;Add More Attributes(s)&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
