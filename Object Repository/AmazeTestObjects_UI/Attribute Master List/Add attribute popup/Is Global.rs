<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Is Global Toggle</description>
   <name>Is Global</name>
   <tag></tag>
   <elementGuidId>ceff96ab-c2d2-4e9a-8618-6f01a48a910e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(),'Is Global')]/following-sibling::p/label/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
