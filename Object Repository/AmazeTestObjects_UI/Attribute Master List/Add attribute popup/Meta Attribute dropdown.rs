<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Meta Attribute dropdown</name>
   <tag></tag>
   <elementGuidId>1b3138e1-ae1c-46f7-9639-ffc7c7482ab0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(text(), 'Select Meta Attribute')]/following-sibling::div//mat-select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
