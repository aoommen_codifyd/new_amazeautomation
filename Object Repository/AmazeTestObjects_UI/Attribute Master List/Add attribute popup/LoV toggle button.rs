<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LoV toggle button</name>
   <tag></tag>
   <elementGuidId>b8b4088b-22e1-4c53-a911-2bbd2bcd41e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(@id,&quot;UAT-attribute-canhaveLOV&quot;)]/following-sibling::span</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
