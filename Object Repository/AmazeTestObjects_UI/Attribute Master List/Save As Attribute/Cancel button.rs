<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Save As Attribute popup Cancel</description>
   <name>Cancel button</name>
   <tag></tag>
   <elementGuidId>0e3136cb-49bd-40f9-88d9-3691046d4b6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[text()='Submit']/parent::span/parent::button/following-sibling::button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
