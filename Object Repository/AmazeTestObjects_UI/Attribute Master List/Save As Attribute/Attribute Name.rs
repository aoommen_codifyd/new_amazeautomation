<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Save As attr name</description>
   <name>Attribute Name</name>
   <tag></tag>
   <elementGuidId>8b0dd5b8-0bfd-450c-8ad7-2302710a0700</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[text()='Attribute Name']/following-sibling::input[@placeholder='Type Attribute Name']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
