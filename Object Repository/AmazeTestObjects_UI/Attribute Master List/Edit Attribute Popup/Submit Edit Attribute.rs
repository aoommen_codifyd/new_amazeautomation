<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Edit Attribute Button on the popup</description>
   <name>Submit Edit Attribute</name>
   <tag></tag>
   <elementGuidId>3434d6e7-734a-4b26-aca2-a2698ab0bc5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(), 'Edit Attribute')]/parent::span/parent::button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
