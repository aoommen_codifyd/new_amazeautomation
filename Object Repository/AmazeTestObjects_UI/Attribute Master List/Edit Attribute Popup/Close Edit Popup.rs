<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Close Attribute Edit Popup</description>
   <name>Close Edit Popup</name>
   <tag></tag>
   <elementGuidId>ab1ea3a2-9b32-477a-bf3d-7795f7f9e2dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'mat-flat-button')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-flat-button</value>
   </webElementProperties>
</WebElementEntity>
