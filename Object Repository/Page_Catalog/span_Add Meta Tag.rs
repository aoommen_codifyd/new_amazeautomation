<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add Meta Tag</name>
   <tag></tag>
   <elementGuidId>fa0e6e61-5ea2-4ea2-9d76-b8b4414df19c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.rounded-pill.my-2.mat-raised-button.mat-button-base.mat-accent.cdk-focused.cdk-mouse-focused > span.mat-button-wrapper > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//mat-dialog-container[@id='mat-dialog-0']/amaze-dialog/mat-dialog-content/app-add-edit-attribute-metatag-popup/div/form/div[2]/button/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Meta Tag</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-dialog-0&quot;)/amaze-dialog[@class=&quot;ng-star-inserted&quot;]/mat-dialog-content[@class=&quot;mat-typography stylishSlimScroll p-0 m-0 mat-dialog-content&quot;]/app-add-edit-attribute-metatag-popup[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;d-block w-100 overflow-hidden mx-auto add-edit-metatag-wrapper&quot;]/form[@class=&quot;ng-dirty ng-valid ng-touched&quot;]/div[@class=&quot;form-btn text-sm-center amaze-bg&quot;]/button[@class=&quot;rounded-pill my-2 mat-raised-button mat-button-base mat-accent cdk-focused cdk-mouse-focused&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//mat-dialog-container[@id='mat-dialog-0']/amaze-dialog/mat-dialog-content/app-add-edit-attribute-metatag-popup/div/form/div[2]/button/span/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Attribute(s)'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add Meta Tag']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-add-edit-attribute-metatag-popup/div/form/div[2]/button/span/span</value>
   </webElementXpaths>
</WebElementEntity>
