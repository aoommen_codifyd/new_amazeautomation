<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mat-toolbar-row_Add Attribute Meta Tagclear</name>
   <tag></tag>
   <elementGuidId>2c13d1d3-fc5d-467a-bcce-6fe9d17c5f42</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#mat-dialog-title-0 > mat-toolbar.mat-toolbar.mat-toolbar-multiple-rows.ng-star-inserted > mat-toolbar-row.mat-toolbar-row</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='mat-dialog-title-0']/mat-toolbar/mat-toolbar-row</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>mat-toolbar-row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-toolbar-row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			Add Attribute Meta Tag
			
			
				clear
			
			
		</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-dialog-title-0&quot;)/mat-toolbar[@class=&quot;mat-toolbar mat-toolbar-multiple-rows ng-star-inserted&quot;]/mat-toolbar-row[@class=&quot;mat-toolbar-row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mat-dialog-title-0']/mat-toolbar/mat-toolbar-row</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete Attribute Meta Tag'])[1]/following::mat-toolbar-row[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit Attribute Meta Tag'])[1]/following::mat-toolbar-row[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Meta Tag Name'])[1]/preceding::mat-toolbar-row[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/mat-toolbar/mat-toolbar-row</value>
   </webElementXpaths>
</WebElementEntity>
