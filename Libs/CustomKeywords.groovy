
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import org.openqa.selenium.WebElement

import java.lang.String

import org.openqa.selenium.WebDriver

import com.kms.katalon.core.testobject.TestObject



def static "com.amaze.utilities.Waits.wait4Clickable"(
    	WebElement element	
     , 	int time	) {
    (new com.amaze.utilities.Waits()).wait4Clickable(
        	element
         , 	time)
}

 /*** --------------------- 1. LOGIN PAGE: Login using the user name and password. ------------------------------ ***/ 
def static "com.amaze.utilities.commonUtilities.login"(
    	String company	
     , 	String username	
     , 	String password	) {
    (new com.amaze.utilities.commonUtilities()).login(
        	company
         , 	username
         , 	password)
}

 /*** ---------------------------- 2. PRODUCT FAMILY: Create a Product family pop-up filling -------------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.createPF"(
    	String pfID	
     , 	String pfTitle	
     , 	String attributeName	) {
    (new com.amaze.utilities.commonUtilities()).createPF(
        	pfID
         , 	pfTitle
         , 	attributeName)
}

 /*** -------------------------- 3. CATALOG LISTING PAGE: Navigate back to catalog listing page ------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.navigateToCatalogListingPage"() {
    (new com.amaze.utilities.commonUtilities()).navigateToCatalogListingPage()
}

 /*** ------------------------  4. Navigate from landing page to a catalog's taxonomy node -------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.navigateToCatalog"(
    	String catalog	
     , 	String taxNode	) {
    (new com.amaze.utilities.commonUtilities()).navigateToCatalog(
        	catalog
         , 	taxNode)
}

 /*** ------------------------  5. Refresh page by click on selected taxonomy node -------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.refreshNode"(
    	String node	) {
    (new com.amaze.utilities.commonUtilities()).refreshNode(
        	node)
}

 /*** ------------------------------------6. Add Schema page operations------------------------------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.schemaPageOp"(
    	String attributeName	
     , 	String lov	
     , 	String custAttrId	) {
    (new com.amaze.utilities.commonUtilities()).schemaPageOp(
        	attributeName
         , 	lov
         , 	custAttrId)
}

 /*** ---------------------------------------------7. Navigate to schema view-----------------------------------  ***/ 
def static "com.amaze.utilities.commonUtilities.navigateToSchemaView"() {
    (new com.amaze.utilities.commonUtilities()).navigateToSchemaView()
}

 /*** -----------------------------------------------8. Delete Schema-------------------------------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.deleteSchema"(
    	String attributeName	) {
    (new com.amaze.utilities.commonUtilities()).deleteSchema(
        	attributeName)
}

 /*** ---------------------------------------------9.Logout--------------------------------------------------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.logout"() {
    (new com.amaze.utilities.commonUtilities()).logout()
}

 /*** ---------------------------------------------10.Add Attribute----------------------------------------------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.addAttributePopUp"(
    	WebDriver driver	
     , 	String attributeName	
     , 	String custAttrId	
     , 	String attrDescTest	
     , 	String dataType	
     , 	String isGlobal	
     , 	String multipleUom	
     , 	String canHaveLov	
     , 	String uomValue	
     , 	String lovValue	) {
    (new com.amaze.utilities.commonUtilities()).addAttributePopUp(
        	driver
         , 	attributeName
         , 	custAttrId
         , 	attrDescTest
         , 	dataType
         , 	isGlobal
         , 	multipleUom
         , 	canHaveLov
         , 	uomValue
         , 	lovValue)
}

 /*** ----------------------------------------11.Upload file------------------------------------------------------- ***/ 
def static "com.amaze.utilities.commonUtilities.uploadFile"(
    	TestObject to	
     , 	String filePath	) {
    (new com.amaze.utilities.commonUtilities()).uploadFile(
        	to
         , 	filePath)
}

 /***----------------------------------------------12.Add Schema-------------------------------------------------***/ 
def static "com.amaze.utilities.commonUtilities.addSchema"(
    	String attrName	
     , 	String custID	
     , 	String schemaDesc	
     , 	String navigationOrder	
     , 	String lovValue	) {
    (new com.amaze.utilities.commonUtilities()).addSchema(
        	attrName
         , 	custID
         , 	schemaDesc
         , 	navigationOrder
         , 	lovValue)
}

 /***---------------------------------13.Navigate to Add DA in DA library----------------------------------------------***/ 
def static "com.amaze.utilities.commonUtilities.navigateToAddDaPage"() {
    (new com.amaze.utilities.commonUtilities()).navigateToAddDaPage()
}

 /***-----------------------------14.Cancel SKU validation failure popup----------------------------------  ***/ 
def static "com.amaze.utilities.commonUtilities.skuValidationFailurePopup"() {
    (new com.amaze.utilities.commonUtilities()).skuValidationFailurePopup()
}

 /***-----------------------------15.Add SKU----------------------------------------------------------------  ***/ 
def static "com.amaze.utilities.commonUtilities.addSKU"(
    	String skuId	
     , 	String title	
     , 	String attrName	
     , 	String attrValue	) {
    (new com.amaze.utilities.commonUtilities()).addSKU(
        	skuId
         , 	title
         , 	attrName
         , 	attrValue)
}

 /***-----------------------------16.Refresh taxonomy panel----------------------------------------------------------------  ***/ 
def static "com.amaze.utilities.commonUtilities.refreshNode"() {
    (new com.amaze.utilities.commonUtilities()).refreshNode()
}

 /*** ------------------------------------Schema Assertion------------------------------------------- ***/ 
def static "com.amaze.utilities.Assertions.schemaAssertion"(
    	String attributeName	) {
    (new com.amaze.utilities.Assertions()).schemaAssertion(
        	attributeName)
}

 /**
	 * Refresh browser
	 */ 
def static "newKeywords.Keys.refreshBrowser"() {
    (new newKeywords.Keys()).refreshBrowser()
}

 /**
	 * Click element
	 * @param to Katalon test object
	 */ 
def static "newKeywords.Keys.clickElement"(
    	TestObject to	) {
    (new newKeywords.Keys()).clickElement(
        	to)
}

 /**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */ 
def static "newKeywords.Keys.getHtmlTableRows"(
    	TestObject table	
     , 	String outerTagName	) {
    (new newKeywords.Keys()).getHtmlTableRows(
        	table
         , 	outerTagName)
}
