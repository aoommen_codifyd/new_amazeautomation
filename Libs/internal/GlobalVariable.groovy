package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object catalog_Id
     
    /**
     * <p></p>
     */
    public static Object rootNode_Name
     
    /**
     * <p></p>
     */
    public static Object node_Id
     
    /**
     * <p></p>
     */
    public static Object AttrName
     
    /**
     * <p></p>
     */
    public static Object gmailID
     
    /**
     * <p></p>
     */
    public static Object gmailPassword
     
    /**
     * <p>Profile AmazeTest : TC- Add Attribute_UI</p>
     */
    public static Object attr_Name
     
    /**
     * <p>Profile AmazeTest : TC- Add attribute. Can be 'DECIMAL', 'INTEGER', 'STRING' or 'DATETIME'</p>
     */
    public static Object dataType
     
    /**
     * <p>Profile AmazeTest : TC- Add attribute. Can be 'true' or 'false'</p>
     */
    public static Object isGlobal
     
    /**
     * <p>Profile AmazeTest : TC- Edit attribute</p>
     */
    public static Object edit_Name
     
    /**
     * <p>Profile AmazeTest : TC- Edit attribute. Can be 'DECIMAL', 'INTEGER', 'STRING' or 'DATETIME'</p>
     */
    public static Object edit_DataType
     
    /**
     * <p>Profile AmazeTest : TC- Edit attribute. Can be true or false</p>
     */
    public static Object edit_isGlobal
     
    /**
     * <p>Profile AmazeTest : TC - edit attribute. Data should be as per the selected data type</p>
     */
    public static Object edit_Lov
     
    /**
     * <p>Profile AmazeTest : TC - Edit Attribute</p>
     */
    public static Object attribute_Id
     
    /**
     * <p>Profile AmazeTest : TC - Save as attribute.</p>
     */
    public static Object saveAs_Name
     
    /**
     * <p>Profile AmazeTest : TC- Save as attribute. Can be 'DECIMAL', 'INTEGER', 'STRING' or 'DATETIME'</p>
     */
    public static Object saveAs_DataType
     
    /**
     * <p>Profile AmazeTest : TC - Save as attribute. Data should be as per the selected data type.</p>
     */
    public static Object saveAs_Lov
     
    /**
     * <p></p>
     */
    public static Object schema_Id
     
    /**
     * <p>Profile AmazeTest : TC - Add SKU</p>
     */
    public static Object sku_Id
     
    /**
     * <p></p>
     */
    public static Object AttrNameAdded
     
    /**
     * <p></p>
     */
    public static Object TaxNode
     
    /**
     * <p></p>
     */
    public static Object newAttr
     
    /**
     * <p></p>
     */
    public static Object LoV_String
     
    /**
     * <p></p>
     */
    public static Object URL
     
    /**
     * <p></p>
     */
    public static Object deleteNode
     
    /**
     * <p></p>
     */
    public static Object pfID_global
     
    /**
     * <p></p>
     */
    public static Object cPfID_global
     
    /**
     * <p></p>
     */
    public static Object company
     
    /**
     * <p></p>
     */
    public static Object catalog
     
    /**
     * <p></p>
     */
    public static Object taxNode
     
    /**
     * <p></p>
     */
    public static Object newNodeName
     
    /**
     * <p></p>
     */
    public static Object editAttrAPI
     
    /**
     * <p></p>
     */
    public static Object attributeNames
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object CustId
     
    /**
     * <p></p>
     */
    public static Object catalogID
     
    /**
     * <p></p>
     */
    public static Object pfID_Global
     
    /**
     * <p></p>
     */
    public static Object UOM_Name
     
    /**
     * <p></p>
     */
    public static Object UOM_Symbol
     
    /**
     * <p></p>
     */
    public static Object UOM_Category
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            catalog_Id = selectedVariables['catalog_Id']
            rootNode_Name = selectedVariables['rootNode_Name']
            node_Id = selectedVariables['node_Id']
            AttrName = selectedVariables['AttrName']
            gmailID = selectedVariables['gmailID']
            gmailPassword = selectedVariables['gmailPassword']
            attr_Name = selectedVariables['attr_Name']
            dataType = selectedVariables['dataType']
            isGlobal = selectedVariables['isGlobal']
            edit_Name = selectedVariables['edit_Name']
            edit_DataType = selectedVariables['edit_DataType']
            edit_isGlobal = selectedVariables['edit_isGlobal']
            edit_Lov = selectedVariables['edit_Lov']
            attribute_Id = selectedVariables['attribute_Id']
            saveAs_Name = selectedVariables['saveAs_Name']
            saveAs_DataType = selectedVariables['saveAs_DataType']
            saveAs_Lov = selectedVariables['saveAs_Lov']
            schema_Id = selectedVariables['schema_Id']
            sku_Id = selectedVariables['sku_Id']
            AttrNameAdded = selectedVariables['AttrNameAdded']
            TaxNode = selectedVariables['TaxNode']
            newAttr = selectedVariables['newAttr']
            LoV_String = selectedVariables['LoV_String']
            URL = selectedVariables['URL']
            deleteNode = selectedVariables['deleteNode']
            pfID_global = selectedVariables['pfID_global']
            cPfID_global = selectedVariables['cPfID_global']
            company = selectedVariables['company']
            catalog = selectedVariables['catalog']
            taxNode = selectedVariables['taxNode']
            newNodeName = selectedVariables['newNodeName']
            editAttrAPI = selectedVariables['editAttrAPI']
            attributeNames = selectedVariables['attributeNames']
            username = selectedVariables['username']
            password = selectedVariables['password']
            CustId = selectedVariables['CustId']
            catalogID = selectedVariables['catalogID']
            pfID_Global = selectedVariables['pfID_Global']
            UOM_Name = selectedVariables['UOM_Name']
            UOM_Symbol = selectedVariables['UOM_Symbol']
            UOM_Category = selectedVariables['UOM_Category']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
