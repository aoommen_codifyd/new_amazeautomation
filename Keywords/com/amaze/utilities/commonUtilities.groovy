package com.amaze.utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions;
import internal.GlobalVariable
import org.openqa.selenium.support.ui.Select;
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import org.openqa.selenium.Keys as Keys


public class commonUtilities {

	/*** --------------------- 1. LOGIN PAGE: Login using the user name and password. ------------------------------ ***/
	@Keyword
	def login(String company, String username, String password){

		WebUI.openBrowser('http://test.codifyd.com/amazeWeb-test/#/login')

		WebUI.maximizeWindow()

		WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/input_companyName'), 3000)

		WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/input_companyName'), company)

		WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/usernameField'), username)

		WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/passwordField'), password)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/LoginButton'))

		WebUI.delay(10)

		WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

		def text = WebUI.getText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Page Text'))

		println(('We are on ' + text) + ' page.')

		boolean result = WebUI.verifyTextPresent('Catalogs', false, FailureHandling.STOP_ON_FAILURE)

		if (result == true) {
			println('Login completed')
		} else {
			println('Login failed')
		}
	}


	/*** ---------------------------- 2. PRODUCT FAMILY: Create a Product family pop-up filling -------------------------- ***/
	@Keyword
	def createPF(String pfID, String pfTitle, String attributeName){

		WebDriver driver = DriverFactory.getWebDriver();

		WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Product Family ID Textbox'), 300)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Product Family ID Textbox'))

		ArrayList pfDetails = []
		pfID = (pfID + new Date().getTime())
		pfDetails.add(pfID)

		if(pfID.contains('cPF')){
			GlobalVariable.cPfID_global =pfID
		}else{
			GlobalVariable.pfID_global = pfID
		}

		WebUI.setText(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Product Family ID Textbox'), pfID)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/PF Title Textbox'))

		pfTitle = (pfTitle + new Date().getTime())
		pfDetails.add(pfTitle)
		WebUI.setText(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/PF Title Textbox'), pfTitle)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Add Existing Schema'))

		//Selecting the schema attribute from the dropdown
		WebElement ulElement = driver.findElement(By.xpath("//ul[@class='item2']"))

		List<WebElement> liList = ulElement.findElements(By.tagName('div'))

		for (WebElement list : liList) {
			if (list.getText().contains(attributeName)) {
				println('Selected value' + list.getText())

				list.click()

				break
			}
		}

		WebUI.delay(2)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Add Existing Schema'))

		WebUI.delay(2)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Copy SKU Attr button'))

		WebUI.delay(2)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Submit PF button'))

		println(('The PF node - ' + pfID) + 'was created.')

		return pfDetails

		WebUI.delay(15)
	}

	/*** -------------------------- 3. CATALOG LISTING PAGE: Navigate back to catalog listing page ------------------- ***/
	@Keyword
	def navigateToCatalogListingPage(){

		WebUI.delay(5)

		WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog link'), 300, FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog link'))

		WebUI.delay(5)
	}

	/*** ------------------------  4. Navigate from landing page to a catalog's taxonomy node -------------------- ***/
	@Keyword
	def navigateToCatalog(String catalog, String taxNode){

		WebDriver driver = DriverFactory.getWebDriver();

		WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 3000)

		WebUI.delay(10)

		//Click on catalog searchbox
		WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

		WebUI.delay(3)

		//Set catalog name
		WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

		WebUI.delay(3)

		//Click on the launch button
		WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

		WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 300)

		WebUI.delay(2)

		//Selecting the panels to display
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Catalog_UI/Pane Selection Button'))

		WebUI.delay(2)

		//Selecting the middle panel

		String middlePanelChKBox = driver.findElement(By.xpath("//div[contains(text(), 'Middle Panel')]/preceding-sibling::mat-pseudo-checkbox")).getAttribute('class')
		if(!middlePanelChKBox.contains('checkbox-checked')){
			println('middleP: ' + middlePanelChKBox)
			driver.findElement(By.xpath("//div[contains(text(), 'Middle Panel')]/preceding-sibling::mat-pseudo-checkbox")).click()
		}

		WebUI.delay(2)

		//Selecting the last panel
		WebElement lastPanelChkBox = driver.findElement(By.xpath("//div[contains(text(), 'Last Panel')]/preceding-sibling::mat-pseudo-checkbox"))
		String lastPanelChk = lastPanelChkBox.getAttribute('class')
		if(!lastPanelChk.contains('checkbox-checked')){
			println('last P: ' + lastPanelChk)
			lastPanelChkBox.click()
		}

		WebUI.delay(2)

		//Clicking away from the popup

		Actions actions = new Actions(driver)
		actions.moveByOffset(300,400).click().perform()

		WebUI.delay(5)

		//Click on the taxonomy search textbox
		WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'))

		WebUI.delay(2)

		//Set search text
		WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), taxNode)

		WebUI.delay(5)

		//click on the taxonomy dropdown
		WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Taxonomy_dropdown'))

		WebUI.delay(5)
	}

	/*** ------------------------  5. Refresh page by click on selected taxonomy node -------------------- ***/
	@Keyword
	def refreshNode(String node){

		WebDriver driver = DriverFactory.getWebDriver();

		//refresh the PF view by clicking on the selected taxonomy node again
		WebElement taxList = driver.findElement(By.xpath("//div[@id='taxonomy-tree']/ul"))

		List <WebElement> spanList = taxList.findElements(By.tagName("span"))

		for(WebElement myNode: spanList){
			if(myNode.getAttribute("title").equals(node)){
				myNode.click()
				break
			}
		}

		WebUI.delay(5)
	}

	/*** ------------------------------------6. Add Schema page operations------------------------------------------- ***/
	@Keyword
	def boolean schemaPageOp(String attributeName, String lov, String custAttrId){

		WebUI.delay(5)

		//Click on the Attribute name textbox
		WebUI.waitForElementPresent(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'), 20)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'))

		WebDriver driver = DriverFactory.getWebDriver()

		//Set text on attribute name textbox
		driver.findElement(By.xpath("//input[@ng-reflect-form-control-name='attrName']")).sendKeys(attributeName)

		WebUI.delay(2)

		//Click on the 	Cust Attr id
		driver.findElement(By.xpath("//input[@formcontrolname='customerAttributeId']")).click()

		WebUI.delay(2)

		driver.findElement(By.xpath("//input[@formcontrolname='customerAttributeId']")).sendKeys(custAttrId)

		//Click on the add lov link
		driver.findElement(By.xpath("//a[contains(text(), 'Add LoVs')]")).click()

		WebUI.delay(2)

		//Click on the LoV textbox
		driver.findElement(By.xpath("//input[@placeholder='values']")).click()

		WebUI.delay(2)

		driver.findElement(By.xpath("//input[@placeholder='values']")).sendKeys(lov)

		WebUI.delay(2)

		//Click on the Add Schema button
		WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Add Schema Submit button'), 20)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Add Schema Submit button'))

		WebUI.delay(8)


	}

	/*** ---------------------------------------------7. Navigate to schema view-----------------------------------  ***/
	@Keyword
	def navigateToSchemaView(){

		WebUI.delay(2)

		//click on the schema view radio button
		WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Schema View Radio Button'), 20)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Schema View Radio Button'))

		WebUI.delay(2)
	}

	/*** -----------------------------------------------8. Delete Schema-------------------------------------------- ***/
	@Keyword
	def deleteSchema(String attributeName){

		WebDriver driver = DriverFactory.getWebDriver();

		//Click on the attribute Name searchbox
		driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).click()

		driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).clear()

		//set the attribute name in the above selected textbox
		driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).sendKeys(attributeName)

		WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

		WebElement selectionBox = driver.findElement(By.xpath(('//td[contains(text(),\'' + attributeName) + '\')]/preceding-sibling::td/input[@role=\'checkbox\']'))

		if (!(selectionBox.isSelected())) {
			//select the searched item
			driver.findElement(By.xpath(('//td[contains(text(),\'' + attributeName) + '\')]/preceding-sibling::td/input[@role=\'checkbox\']')).click()
		}

		WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

		//Click on the delete button
		driver.findElement(By.xpath('//button[@mattooltip=\'Delete Schema(s)\']')).click()

		WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

		//Confirm Schema attribute deletion
		driver.findElement(By.xpath('//button[@class=\'rounded-pill px-4 mat-raised-button mat-button-base mat-accent\']')).click()

		WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)
	}

	/*** ---------------------------------------------9.Logout--------------------------------------------------------------- ***/
	@Keyword
	def logout(){

		WebUI.delay(5)

		WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Logout/Logout link'), 200)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Logout/Logout link'))

		WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Logout/Logout button'), 200)

		WebUI.click(findTestObject('AmazeTestObjects_UI/Logout/Logout button'))

		WebUI.delay(5)

		WebUI.closeBrowser()
	}

	/*** ---------------------------------------------10.Add Attribute----------------------------------------------------------- ***/

	@Keyword
	def addAttributePopUp(WebDriver driver, String attributeName, String custAttrId, String attrDescTest, String dataType, String isGlobal, String multipleUom, String canHaveLov, String uomValue, String lovValue){

		//Click on the Attr name textbox
		WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'))

		WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'), 300)

		//clear attribute name textbox
		WebUI.clearText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'))


		String attrName = ((attributeName) + new Date().getTime())

		//Set attr name
		WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'), attrName)

		//set customer attr ID
		WebElement custAttrIDInput = driver.findElement(By.xpath("//input[contains(@placeholder,'Type Customer Attribute Id')]"))
		custAttrIDInput.click()
		String custId = custAttrId + new Date().getTime()
		custAttrIDInput.sendKeys(custId)

		//Set Attr description
		WebElement attrDesc = driver.findElement(By.xpath("//textarea[contains(@placeholder,'Type Attribute Description')]"))
		attrDesc.click()
		attrDesc.sendKeys(attrDescTest)

		//set datatype
		Select datatypedd = new Select(driver.findElement(By.xpath("//select[contains(@id, 'UAT-attribute-datatype-0')]")))
		datatypedd.selectByValue(dataType)

		//if derived attr then select the formula
		if(dataType.equalsIgnoreCase("DERIVED")){

			WebUI.delay(1)

			//Click formula input box
			driver.findElement(By.xpath("//input[contains(@placeholder, 'Select Formula')]")).click()

			WebUI.delay(2)

			//Select the formula
			driver.findElement(By.xpath("//td[contains(@title,'test22')]")).click()

			WebUI.delay(2)

			//click on the select formula button
			driver.findElement(By.xpath("//button[@id='UAT-attribute-select-formula-proceed']")).click()

			WebUI.delay(2)
		}

		//is Global?
		if (isGlobal.equalsIgnoreCase('Y')) {
			WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Is Global'))
		}

		//can have multiple UoMs
		if(multipleUom.equalsIgnoreCase('Y')){
			driver.findElement(By.xpath("//input[contains(@ng-reflect-name,'attribute.canHaveMultipleUom')]/following-sibling::span")).click()
		}

		//can have LoV
		if(canHaveLov.equalsIgnoreCase('Y')){
			driver.findElement(By.xpath("//input[contains(@id,'UAT-attribute-canhaveLOV')]/following-sibling::span")).click()

			WebUI.delay(2)

			//Click on UoM div
			driver.findElement(By.xpath("//span[contains(text(),'Add UoM(s)')]/parent::div")).click()

			WebUI.delay(1)

			//Select the UoM
			WebElement uomInput = driver.findElement(By.xpath("//input[contains(@placeholder,'Type UoM')]"))
			uomInput.click()
			uomInput.sendKeys(uomValue)
			WebUI.delay(2)
			driver.findElement(By.xpath("//span[contains(text(), 'cm')]/parent::mat-option")).click()

			WebUI.delay(1)

			//Set LoV
			//click on the LoV div
			driver.findElement(By.xpath("//span[contains(text(),'Add LoV(s)')]/parent::div")).click()

			WebUI.delay(1)

			//click on the Lov value input box
			WebElement lovInput = driver.findElement(By.xpath("//input[contains(@id,'UAT-attribute-LOV')]"))
			lovInput.click()

			//set LoV value
			lovInput.sendKeys(lovValue)

			//Select UoM for LoV values
			Select uomSelector = new Select(driver.findElement(By.xpath("//select[contains(@id,'UAT-attribute-selectUOM')]")))
			uomSelector.selectByVisibleText(uomValue)
		}

		WebUI.delay(2)
	}


	/*** ----------------------------------------11.Upload file------------------------------------------------------- ***/

	@Keyword
	def uploadFile(TestObject to, String filePath) {

		println("filePath: " + filePath)
		String newPath = filePath.replace('/', '\\')
		WebUI.click(to)
		WebUI.delay(3) //Delay after click on Browser Button
		StringSelection ss = new StringSelection(newPath);
		println(newPath)
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss,null);
		WebUI.delay(1) //Delay after paste the text
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	/***----------------------------------------------12.Add Schema-------------------------------------------------***/

	@Keyword
	def addSchema(String attrName, String custID, String schemaDesc, String navigationOrder, String lovValue){

		WebUI.delay(1)

		//click on the schema view tab
		WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Schema View Radio Button'), 300,
				FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Schema View Radio Button'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

		//Click on the Add schema button
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Button'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

		//Click on the Attribute name textbox
		WebUI.waitForElementPresent(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'),
				200, FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'), FailureHandling.CONTINUE_ON_FAILURE)


		String attributeName = attrName + new Date().getTime()

		ArrayList schemaDetails = []
		schemaDetails.add(attributeName)

		//Set text on attribute name textbox
		WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'), attributeName)

		WebUI.delay(2)

		//Click on the Customer id input
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Customer ID Input'))

		//Set Customer ID
		String customerID = custID + new Date().getTime()
		WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Customer ID Input'), customerID)

		schemaDetails.add(customerID)

		//Click on schema description
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Schema Description Textarea'))

		//Set Schema description
		WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Schema Description Textarea'), schemaDesc)

		//Click on navigation order
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Navigation order inputbox'))

		//Set navigation order
		WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Navigation order inputbox'), navigationOrder)

		//Add LoV
		//Click on the add lov link
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Add LoV link'))

		WebUI.delay(1)

		//Click on the LoV textbox
		//WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Add LoV link'))

		WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/LoV TextBox'), lovValue)

		//Click on the Add Schema button
		WebUI.delay(2)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Add Schema Submit button'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

		return schemaDetails
	}

	/***---------------------------------13.Navigate to Add DA in DA library----------------------------------------------***/

	@Keyword
	def navigateToAddDaPage(){

		//wait for menu icon
		WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/menu icon'), 100, FailureHandling.CONTINUE_ON_FAILURE)

		//click on the menu icon
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/menu icon'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

		//scroll to DA link
		WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Digital Assets link'), 100)

		//Click on the DA library link
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Digital Assets link'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Digital Asset Library Link'))

		WebUI.delay(2)

		//wait for add DA button
		WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Add DA button'),
				200, FailureHandling.CONTINUE_ON_FAILURE)

		//Click on the Add new DA button
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Add DA button'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	}

	/***-----------------------------14.Cancel SKU validation failure popup----------------------------------  ***/

	@Keyword
	def skuValidationFailurePopup() {

		boolean validation_present = WebUI.verifyElementPresent(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), 300)

		println("SKU validation_present: "+ validation_present)

		if(WebUI.verifyElementPresent(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), 300)) {
			WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), 300)
			WebUI.delay(2)
			WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'))
		}
	}

	/***-----------------------------15.Add SKU----------------------------------------------------------------  ***/

	@Keyword
	def addSKU(String skuId, String title, String attrName, String attrValue) {

		WebDriver driver = DriverFactory.getWebDriver()

		//Click on the add SKU button
		WebElement addSkuButton = driver.findElement(By.xpath('//button[@mattooltip=\'Add SKU\']'))

		addSkuButton.click()

		WebUI.delay(3)

		//Click on the add SKU expansion panel
		driver.findElement(By.xpath('//mat-expansion-panel[@id=\'add-sku-0\']')).click()

		WebUI.delay(2)

		//Click on the SKU item Id text
		WebElement skuItemId = driver.findElement(By.xpath('//label[text() = \'SKU Item ID\']/following-sibling::input'))

		skuItemId.click()

		WebUI.delay(1)

		String newSKUId = skuId + new Date().getTime()

		//Set SKU item id
		skuItemId.sendKeys(newSKUId)

		//Click on the SKU title input
		WebElement skuTitle = driver.findElement(By.xpath('//input[contains(@placeholder,\'Enter SKU Title\')]'))

		//if SKU title is enabled
		if (skuTitle.isEnabled()) {
			skuTitle.click()

			WebUI.delay(1)

			//Set skuTitle
			skuTitle.sendKeys(title + new Date().getTime())
		}

		WebUI.delay(1)

		/*** Add attribute manually ***/
		WebUI.delay(2)

		//Click on the attriburte name input box
		WebElement attrNameInput = driver.findElement(By.xpath('//input[contains(@placeholder,\'Type Attribute Name\')]'))

		attrNameInput.click()

		//Set attrName in the attr name input
		attrNameInput.sendKeys(attrName)

		WebUI.delay(5)

		WebElement attrList = driver.findElement(By.xpath('//div[@role=\'listbox\']'))

		List<WebElement> listAttr = attrList.findElements(By.tagName('mat-option'))

		for (WebElement iList : listAttr) {
			if (iList.getAttribute('title').equals(attrName)) {
				iList.click()

				break
			}
		}

		WebUI.delay(3)

		//double click to add attribute value
		WebUI.doubleClick(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Attribute value double-click'))

		WebUI.delay(3)

		//click on the value text field
		WebUI.doubleClick(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Attribute value field'))

		//send value to the textarea
		WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Add Value text area'), attrValue)

		//click on the enter button
		WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Add Value text area'), Keys.chord(Keys.ENTER))

		WebUI.delay(2)

		//Click on the Add button
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Attribute Value Add Button'))

		WebUI.delay(2)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Add SKU button'))

		WebUI.delay(8)

		return newSKUId;

	}

	/***-----------------------------16.Refresh taxonomy panel----------------------------------------------------------------  ***/

	@Keyword
	def refreshNode() {
		WebUI.delay(5)

		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Refresh Node'))

		WebUI.delay(2)
	}
}

