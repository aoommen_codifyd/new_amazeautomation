Codifyd Amaze Automation Project
---------------------------------
_________________________________

Author: Allwyn Oommen
Company: Codifyd Inc.
Test System: Amaze Test

Steps:
------
1. The following project has been designed in  Katalon Studio (). The user has to download Katalon Studio to run the tests. Download Katalon Studio from the following link: https://www.katalon.com
2. Signup to create a Katalon Studio account.
3. Clone this project from the bitbucket repository - https://aoommen_codifyd@bitbucket.org/vivekrs/amaze_automation.git
4. Open the above cloned project from Katalon Studio.
5. Tests are contained in the Test Cases folder.
6. Objects reside in the Object Repository folder.
7. Test Data can be set under Test Data folder.

