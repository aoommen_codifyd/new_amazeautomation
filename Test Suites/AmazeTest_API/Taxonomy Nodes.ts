<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Taxonomy Nodes</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b2efb547-c57f-4f10-b024-a4a590b2a36c</testSuiteGuid>
   <testCaseLink>
      <guid>f0047729-3311-4cc1-bfa5-d1d40c8d18e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Taxonomy Nodes/Add_Bulk Root Node_Delete_Node</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a36b7ae-b9fb-42b5-b02b-fe0969566905</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Taxonomy Nodes/Add_Root_Node</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ac8dcc51-0164-4a56-9e94-28389b103462</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c87818c1-f69f-44e1-bc0a-173739eb9579</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0a32ab95-105e-4532-9dc1-b44afa26b0b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Taxonomy Nodes/Delete_Node</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
