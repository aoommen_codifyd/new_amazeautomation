<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Schema Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b72bb5e6-ce9e-465f-a201-7a6293cde2fc</testSuiteGuid>
   <testCaseLink>
      <guid>6c5b1d56-d154-4409-b407-aa053f613184</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Schema/Add Schema</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cc1bc65-74de-40f8-8906-a0a02ebcfca2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Schema/Delete Schema</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>250b09a7-0347-45b4-bba0-04ef55b237db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Schema/Edit Schema</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
