<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Attributes Master List test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c944453c-f7e7-40d5-b2d9-47d2d53954cf</testSuiteGuid>
   <testCaseLink>
      <guid>adca0a71-baa8-46ba-aa7d-2d8a0153ee45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Attributes Master List/Add Attribute</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4ff99ea0-9e6a-4847-94c7-2a0c3aeb49be</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>81bc78ac-1dc1-44e9-a0f8-4648aa29bde3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Attributes Master List/Edit Attribute</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d01dba7-d881-4595-83b3-93f51f0f86b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Attributes Master List/Delete Attribute</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f919126f-3f54-4312-9da0-8153b63826d0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3a5f4524-b4b9-4a13-89be-b30dcd7415f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Amaze_API/Attributes Master List/Save as attribute</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
