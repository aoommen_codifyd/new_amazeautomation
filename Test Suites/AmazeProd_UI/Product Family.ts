<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Product Family</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>aoommen@codifyd.com;</mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1af5189d-022d-442b-924f-fb357b335b6e</testSuiteGuid>
   <testCaseLink>
      <guid>688b11c1-b8dc-46a0-a066-a0f92123a4e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMaze_UI/Product Family/Add and Delete PF</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>06a57a52-ed84-4cbc-88ab-b351e2c0e830</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>833f50bf-9a82-4754-834f-ddf89aa6e76a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e92248fa-ddff-4207-974e-ae11af4af041</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2a8686ed-2495-4eb0-988f-b693f8791f55</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c0007a4b-7e01-4d4a-b2bb-f82effc8dcdd</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cb621497-6acc-421e-af86-881ac6339595</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fe4768f0-2442-476e-9d17-c1e749f7e0d2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ddef4baf-39b3-4f81-ac64-82da1ccb1225</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c4b82aea-c0f2-4af7-9828-993ee7a641f6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6717c6fe-e7c4-46af-8781-327220325315</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>59433b85-e3ad-41cb-bd45-33972aa43863</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/AMaze_UI/Product Family/Deleting a root PF</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6c80b0fc-b62a-47fc-adae-74f3e0bd5647</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>de22c74b-b88c-4b56-bded-f50d1c8f078c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4bff52d3-3d86-428c-aa1a-1c5b447a0e87</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c9b0b2f7-c594-4edf-89f8-654f2d480a3e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f7e03b87-853c-411d-a9ee-6bd5ab6ae231</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>84431c95-e416-424b-910c-381740bcc12a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a27488d0-3f0e-431c-b970-3c2d6dbd6abe</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8264e922-b687-4e45-a728-034a2331adf0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7e614fd0-03d9-4bf6-a6b0-d5252b85a38f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1eeca34d-448f-4403-8654-84ca0bab8b8d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b97d4e1a-f869-4c23-b3c6-7c32b1316b96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMaze_UI/Product Family/Link and Delink DA to PF</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a3868d43-cf1d-49bd-a6c6-7d7e3109e2ae</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fcf1f78c-24cd-4878-abdb-69f950fec7ab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>12f3ef2d-21e6-4b4d-ad22-7efa299da37e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f8268968-8959-4464-8448-fbb60c938d1e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c00b74c1-d433-49d0-ac1e-d6fb2c4b809c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>322f3ef6-9847-484b-bf01-83ae1f1d1c55</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6e1e744b-b0dc-4397-aad7-f605bfa4ccce</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9d06e4cd-535a-42de-b547-b06c28a542e3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f3657aa8-6efb-402d-a79c-8bac85ffd78c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9c757476-c372-4682-92b2-30cf08553a5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMaze_UI/Product Family/Move PF</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a1a7d4bb-f68b-4493-a5d7-433b0f6386b4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a90dc574-c57e-49c8-8c1a-38d42b6c0776</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bbe83af8-351e-4d0c-918c-eac2585861ed</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7062818f-9406-4b51-8d86-b880cf33dda7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>05eb4419-74ce-4612-b436-6f24bc5f1084</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9c9fd95e-a568-4ee3-bfbf-3c9bf15d79c6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>21967e44-b5f6-4dc0-9129-55d6ba727fb7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>db3cece5-0d64-4ef5-9266-8e28de47c322</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>20575a37-552b-492a-ab05-9c87baedec3a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>833992d0-dabc-408f-b242-f6640ebb0c0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AMaze_UI/Product Family/Manage PF page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0871b4a0-336b-4bc3-8a25-15e2a4624c5f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2dcee895-0512-41aa-9a77-819719f9a0e8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee63451c-b34f-4dd6-a2bd-5bd66d3cc02e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>92642371-6970-477a-94d8-7d2cd49dbd48</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f78fc756-5cf0-4fdc-ac64-a0b29f84a013</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>55dce3f3-e3f7-4dc0-b0dd-1c31e99e736b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d5a720e3-975a-431f-9aa7-f0948b96b624</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>faa96d57-5c65-48aa-ab5c-6067bf309830</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e79a81e9-7045-4cb4-918b-6f73f86f490d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
