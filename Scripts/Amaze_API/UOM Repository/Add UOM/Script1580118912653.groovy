import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response1 = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/UOM/Add UOM', [('UOM_Name') : GlobalVariable.UOM_Name
            , ('UOM_Symbol') : GlobalVariable.UOM_Symbol, ('UOM_Category') : GlobalVariable.UOM_Category]))

println('... Content Type '+ response1.responseText)

if(response1.responseText.contains('already exists in UoM repository')){
	println('... This UOM is already added ... \n Please add a new UOM \n END ')
}else{
	println(GlobalVariable.UOM_Name + ' has been added to the UOM repository.')
}


