import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.testng.Assert as Assert
import org.testng.asserts.Assertion as Assertion
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

List file = fileType

Iterator it = file.iterator()

def slurper = new groovy.json.JsonSlurper()

while(it.hasNext()){

	String ftype = it.next()
	
	println("File Type: "+ ftype)

	exportName = exportName + new Date().getTime()

	response = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Export/Amaze Catalog Export', [('catalog_Id') : GlobalVariable.catalog_Id
		, ('exportName') : exportName, ('fileType') : ftype, ('nodeId') : nodeID]))

	def result = slurper.parseText(response.getResponseBodyContent())

	def export_Id = result.id

	println('export_Id: ' + export_Id)
	
	String state = ''
	
	while (!(state.equals('SUCCESS'))) {
		statusResponse = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Export/Get Export By ID', [('export_ID') : export_Id]))
	
		def result1 = slurper.parseText(statusResponse.getResponseBodyContent())
	
		state = result1.status
	
		println('Current status: ' + state)
	
		WebUI.delay(15)
	}
	
	if (state.equals('SUCCESS')) {
		Assert.assertTrue(true)
	
		println('Export- '+exportName+' of file type '+ftype+' Completed!')
	} else if (state.equals('ERROR')) {
		Assert.assertTrue(false)
	
		println('Export- '+exportName+' of file type '+ftype+' Failed!')
	}

}


	


