import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : 'dev001', ('username') : 'allwyn', ('password') : 'allwyn1234'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('AmazeTestObjects_APIs/Nodes/Add_Nodes/CatalogListing_Search'), 'TestCatalog_Allwyn')

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_APIs/Nodes/Add_Nodes/Catalog_Launch_Button'))

WebUI.delay(5)

WebUI.focus(findTestObject('AmazeTestObjects_APIs/Nodes/Delete_Node/Page_Catalog/span_check_box_outline_blank'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('AmazeTestObjects_APIs/Nodes/Delete_Node/Page_Catalog/span_check_box_outline_blank'), FailureHandling.OPTIONAL)

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_APIs/Nodes/Delete_Node/Page_Catalog/span_check_box_outline_blank'), FailureHandling.OPTIONAL)

WebUI.check(findTestObject('AmazeTestObjects_APIs/Nodes/Delete_Node/Page_Catalog/span_check_box_outline_blank'))

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_APIs/Nodes/Delete_Node/Page_Catalog/button_delete_foreverDelete No'))

WebUI.click(findTestObject('AmazeTestObjects_APIs/Nodes/Delete_Node/Page_Catalog/button_Yes I Confirm'))

