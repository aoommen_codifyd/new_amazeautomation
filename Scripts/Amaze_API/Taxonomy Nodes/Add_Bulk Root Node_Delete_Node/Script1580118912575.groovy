import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response1 = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Nodes/Add_Nodes/Add Bulk Root Nodes', [('catalog_Id') : GlobalVariable.catalog_Id
            , ('rootNode_Name') : 'Root_' + new Date().getTime(), ('rootNode_Name2') : 'Node_' + new Date().getTime()]))

WS.delay(2)

def slurper = new groovy.json.JsonSlurper()

def result = slurper.parseText(response1.getResponseBodyContent())

def value = result.id

println('...extracted value is ' + value)
for (nodeKey in value) {
	WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Nodes/Delete_Node/Delete_node', [('catalog_Id') : GlobalVariable.catalog_Id
		, ('node_Id') : nodeKey]))
}

//GlobalVariable.node_Id = value
//println('--- global variable is ---' + GlobalVariable.node_Id)
//WS.delay(2)
/*WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Nodes/Add_Nodes/Get_Nodes', [('catalog_Id') : GlobalVariable.catalog_Id
            , ('node_Id') : value]))*/



