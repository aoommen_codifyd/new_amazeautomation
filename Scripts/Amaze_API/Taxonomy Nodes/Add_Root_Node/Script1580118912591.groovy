import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : 'dev001', ('username') : 'allwyn', ('password') : 'allwyn1234'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('AmazeTestObjects_APIs/Nodes/Add_Nodes/CatalogListing_Search'), Catalog_Name)

WebUI.click(findTestObject('AmazeTestObjects_APIs/Nodes/Add_Nodes/Catalog_Launch_Button'))

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_APIs/Nodes/Add_Nodes/Add_Node_Button'))

WebUI.delay(2)

WebUI.focus(findTestObject('Page_Catalog/span_Add Root Node'), FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Page_Catalog/span_Add Root Node'), FailureHandling.OPTIONAL)

WebUI.delay(2)

WebUI.click(findTestObject('Page_Catalog/span_Add Root Node'))

WebUI.setText(findTestObject('Object Repository/Page_Catalog/input_Node Name_form-control f'), Root_Node)

WebUI.click(findTestObject('Object Repository/Page_Catalog/button_Add'))

