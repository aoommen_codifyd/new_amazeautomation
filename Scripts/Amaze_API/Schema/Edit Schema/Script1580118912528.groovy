import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*** Get Schema Attributes***/
response = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Schema/Get Schema Attriutes'))

def slurper = new groovy.json.JsonSlurper()

def result = slurper.parseText(response.getResponseBodyContent())

def value = result.results[0].id

GlobalVariable.schema_Id = value

println('...schema Id ' + GlobalVariable.schema_Id)

/*** Edit Schema Attribute***/
WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Schema/Edit Schema', [('catalog_Id') : GlobalVariable.catalog_Id
            , ('schema_Id') : GlobalVariable.schema_Id]))

