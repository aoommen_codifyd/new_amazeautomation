import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.sun.media.sound.SoftReverb.Delay as Delay
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.entity.global.GlobalVariableEntity as GlobalVariableEntity
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

response = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Attributes Master List/Add Attributes', [('catalog_Id') : GlobalVariable.catalog_Id
            , ('attr_Name') : GlobalVariable.attr_Name + new Date().getTime(), ('data_Type') : GlobalVariable.dataType]))

def slurper = new groovy.json.JsonSlurper()

def result = slurper.parseText(response.getResponseBodyContent())

def name = result.name

def custId = result.customerAttributeId

System.out.println('Response: ' + (name[0]) + " "+ 'customerAttributeId: '+ custId[0])

GlobalVariable.newAttr = (name[0])
GlobalVariable.CustId = custId[0]

WebUI.delay(5)

