import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response1 = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Attributes Master List/Add Attributes', [('catalog_Id') : GlobalVariable.catalog_Id
            , ('attr_Name') : (GlobalVariable.attr_Name + '_') + new Date().getTime(), ('data_Type') : GlobalVariable.dataType]))

def slurper = new groovy.json.JsonSlurper()

def result = slurper.parseText(response1.getResponseBodyContent())

def value = result.name

println('... The newly created attribute name is ' + (value[0]))

response2 = WS.sendRequestAndVerify(findTestObject('AmazeTestObjects_APIs/Attributes Master List/Save as attribute', [('catalog_Id') : GlobalVariable.catalog_Id
            , ('saveAs_Name') : GlobalVariable.saveAs_Name + '_' + new Date().getTime(), ('saveAs_DataType') : GlobalVariable.edit_DataType, ('saveAs_Lov') : GlobalVariable.saveAs_Lov]))

def slurper1 = new groovy.json.JsonSlurper()

def result1 = slurper1.parseText(response2.getResponseBodyContent())

def valueSaveAs = result1.name

println('...Save as Attribute ' + valueSaveAs[0])
