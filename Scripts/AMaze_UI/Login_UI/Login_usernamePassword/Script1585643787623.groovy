import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

println('Opening ' + GlobalVariable.URL)

WebUI.openBrowser(url)

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/input_companyName'), 30)

WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/input_companyName'), company)

WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/usernameField'), username)

WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/passwordField'), password)

WebUI.click(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/LoginButton'))

WebUI.delay(10)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

def text = WebUI.getText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Page Text'))

println(('We are on ' + text) + ' page.')

boolean result = WebUI.verifyTextPresent('Catalogs', false, FailureHandling.STOP_ON_FAILURE)

if (result == true) {
    println('Login completed')
} else {
    println('Login failed')
}

