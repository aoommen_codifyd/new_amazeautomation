import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

WebUI.openBrowser(url)

WebUI.maximizeWindow()

WebUI.delay(5)

WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/input_companyName'), 60)

WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/input_companyName'), company)

WebUI.click(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/Google_loginButton'))

WebUI.delay(5)

WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/google_username'), 300)

boolean title = WebUI.getText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/Signin_Page'), FailureHandling.OPTIONAL)

println(title)

if (title == 'Sign In') {
    WebUI.click('AmazeTestObjects_UI/Page_AMAZE  Login/UserAnotherAccount')

    WebUI.delay(2)

    WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/google_username'), 300)
}

WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/google_username'), gmailID)

WebUI.delay(2)

//WebUI.click(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/GUname_nextButton'))

WebUI.delay(2)

WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/google_password'), 300)

WebUI.delay(2)

WebUI.setText(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/google_password'), gmailPassword)

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_UI/Page_AMAZE  Login/GPassword_nextButton'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 40)

def text = WebUI.getText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Page Text'))

println(('We are on ' + text) + ' page.')

boolean result = WebUI.verifyTextPresent('Catalogs', false, FailureHandling.STOP_ON_FAILURE)

if (result == true) {
	println('Login completed')
} else {
	println('Login failed')
}
