import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot as Robot
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import java.awt.event.KeyEvent as KeyEvent
import org.openqa.selenium.Keys as Keys

//login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToAddDaPage'()

//Click on the Upload through URL option
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Upload URL tab'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

String nameDA = url

WebUI.delay(5)

//Click on the url textbox
WebUI.doubleClick(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Add URL Textbox'))

WebUI.delay(2)

//send URL link
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Add url textarea'), nameDA)

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Add url textarea'), Keys.chord(
        Keys.ENTER))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Upload All button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//Assertion
//Navigate to list view
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/DA list view'), 500, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/DA list view'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Sort asset by uploaded date
WebUI.click(findTestObject('AmazeTestObjects_UI/Digital Assets/Add DA/Asset Name Sorting button'))

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Recently Uploaded Date Sorting button'))
WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//hit refresh button while DA is uploading
for (int i = 0; i < 3; i++) {
    WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Refresh Button'))

    WebUI.delay(5)
}

//select asset
WebDriver driver = DriverFactory.getWebDriver()

driver.findElement(By.xpath(('//td[contains(@title,\'' + assetName) + '\')]/preceding-sibling::td/input')).click()

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Select DA options
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/DA options'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Delete DA
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Delete DA button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Confirm delete
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Confirm Delete'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//Navigate back to catalog listing page
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/menu icon'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Menu Catalog Link'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Menu All Catalog link'))

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

