import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent

//login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

//navigate to Add DA page in the DA library
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToAddDaPage'()

//Click on the upload Folder option tab.
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Upload Zip folder'))

WebUI.delay(1)

String folderName = folder_Name

WebUI.delay(1)

//upload selected file
CustomKeywords.'com.amaze.utilities.commonUtilities.uploadFile'(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Browse Zip File'), (RunConfiguration.getProjectDir() +
		'\\Data Files\\DA test\\') + folderName)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Upload All button'), 200)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Add DA/Upload All button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

ArrayList files = folderFiles

//Assertion
for(int i=0; i<files.size(); i++){

	//Navigate to list view
	WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/DA list view'), 500, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/DA list view'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	//Click on the asset name search
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Asset Name Search'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.clearText(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Asset Name Search'), FailureHandling.CONTINUE_ON_FAILURE)

	//Send keys to searchbox
	WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Asset Name Search'), files[i], FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

	//hit refresh button while DA is uploading
	for(int j=0;j<3;j++){
		WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Refresh Button'))
		WebUI.delay(5)
	}

	//select asset
	WebDriver driver = DriverFactory.getWebDriver()

	driver.findElement(By.xpath("//td[contains(@title,'" + files[i] + "')]/preceding-sibling::td/input")).click()

	WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

	//Select DA options
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/DA options'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

	//Delete DA
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Delete DA button'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

	//Confirm delete
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Confirm Delete'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)
}

//Navigate back to catalog listing page and logout
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/menu icon'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Menu Catalog Link'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Digital Assets/Menu All Catalog link'))

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()