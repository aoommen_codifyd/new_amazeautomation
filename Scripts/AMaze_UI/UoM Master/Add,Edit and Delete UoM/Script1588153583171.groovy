import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
//sel
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

//Login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

//Wait for page load
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//click on the quick links button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/UoM Master/Quick Links Button'))

//Click on the UoM Master button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/UoM Master/UoM Master Button'))

WebUI.delay(2)

//wait
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/UoM Master/Add More UoM Button'), 300)

//Click on teh Add more UoM button
WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/Add More UoM Button'))

//wait
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Name Textbox'), 300)

//click on the uom name text box
WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Name Textbox'))

uomName = (uomName + new Date().getTime())

//set the uom name
WebUI.setText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Name Textbox'), uomName)

//set uom symbol
WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Symbol Textbox'))

uomSymbol = (uomSymbol + new Date().getTime())

WebUI.setText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Symbol Textbox'), uomSymbol)

//set uom category
WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM CategoryTextbox'))

WebUI.setText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM CategoryTextbox'), uomCategory)

WebDriver driver = DriverFactory.getWebDriver()

WebElement drop = driver.findElement(By.xpath(('//div[@role=\'listbox\']/mat-option/span[contains(text(), ' + uomCategory) + 
        ')]'))

drop.click()

WebUI.delay(2)

//Click on the Add button
WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/Add Button'))

println(((((('Uom with name - ' + uomName) + ', symbol - ') + uomSymbol) + 'and type - ') + uomCategory) + ' was added.')

WebUI.delay(15)

//edit uom
//search for uom
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Searchbox'), 300)

WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Searchbox'))

WebUI.setText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Searchbox'), uomName)

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/UoM Master/UoM options button'), 300)

//Click on the UoM options
WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM options button'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/UoM Master/Edit UoM Button'), 300)

//click on edit uom button
WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/Edit UoM Button'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Name Textbox'), 300)

WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Name Textbox'))

WebUI.clearText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Name Textbox'))

newUomName = (newUomName + new Date().getTime())

WebUI.setText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Name Textbox'), newUomName)

WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/Edit Submit Button'))

println((('The Uom with name - ' + uomName) + ' was edited to new UoM name - ') + newUomName)

WebUI.delay(5)

//Delete Uom
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Searchbox'), 300)

WebUI.delay(5)

WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Searchbox'))

WebUI.clearText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Searchbox'))

WebUI.setText(findTestObject('AmazeTestObjects_UI/UoM Master/UoM Searchbox'), newUomName)

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/UoM Master/UoM options button'), 300)

WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/UoM options button'))

WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/Delete UoM Button'))

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/UoM Master/Confirm Delete UoM'), 300)

WebUI.click(findTestObject('AmazeTestObjects_UI/UoM Master/Confirm Delete UoM'))

println(('Uom - ' + newUomName) + 'deleted sucessfully!')

WebUI.delay(5)

//navigate to the Catalog page
WebUI.callTestCase(findTestCase('Test/Navigate to Catalog Landing page'), [:], FailureHandling.STOP_ON_FAILURE)

//logout
CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

