import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

//login using the username and password
//CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
		, ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

//Navigate to the taxonomy and a taxonomy node.
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, node)

//wait for PF toogle button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), 3000, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the PF toggle tab
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

/*** ---------------------------------------- Adding a root PF ----------------------------------------------------- ***/
//wait for element - add button, to be clickable
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), 300, 
    FailureHandling.CONTINUE_ON_FAILURE)

//Click on the add PF button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Add Root Node button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add Root PF Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//fill the create new PF pop-up
ArrayList pfDetails = CustomKeywords.'com.amaze.utilities.commonUtilities.createPF'(pfID, pfTitle, attributeName)
String pfID = pfDetails[0]

println("pfID: "+ pfID)

WebUI.delay(10)

//finding the Root PF
WebDriver driver = DriverFactory.getWebDriver()

//click on the new created PF
driver.findElement(By.xpath("//td[@title='"+pfID+"']")).click()

WebUI.delay(1)

//wait for element - add button, to be clickable
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), 300, 
    FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Asset linkage button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Asset Linkage Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Link DA button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Link Asset'), FailureHandling.CONTINUE_ON_FAILURE)

//Wait for Asset name search box
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'), 300, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the asset name search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'), FailureHandling.CONTINUE_ON_FAILURE)

//Type the asset name
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'), assetName, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Select the DA checkbox
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Select DA Checkbox'), 200, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Select DA Checkbox'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//click on the clear filter link
//WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Link DA Clear Text'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Asset Linkage Next Button'), 300)

//Click on the DA next button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Asset Linkage Next Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the submit DA link button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Submit Link DA'), FailureHandling.CONTINUE_ON_FAILURE)

println(((('Digital Asset - ' + assetName) + ' has been attached to ') + GlobalVariable.pfID_global) + ' PF.')

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

/*** ---------------------------------------Delink the DA------------------------------------------ ***/
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Asset Linkage Button'), 
    30)

//Click on the Asset linkage button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Asset Linkage Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Delink DA button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Delink DA button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Select Delink DA selection checkbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Link DA/Delink DA selection Checkbox'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the submit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Link DA/Submit Link DA'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

/*** --------------------------------------------Data clean up --------------------------------------------------***/
//wait fro the delete PF button
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Delete PF/Delete PF Button'), 300)

//Click on the delete pf button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Delete PF/Delete PF Button'))

//Click on confirm delete
WebUI.delay(1)
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Delete PF/Delete PF confirm Button'))

WebUI.delay(10)

/*** --------------------------------------------logout--------------------------------------------------***/

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

