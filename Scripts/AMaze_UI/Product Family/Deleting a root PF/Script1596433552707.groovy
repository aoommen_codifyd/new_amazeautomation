import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.testng.asserts.SoftAssert as SoftAssert

//login using the username and password
not_run: CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

//Navigate to the taxonomy and a taxonomy node.
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, node)

//wait for PF toogle button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), 300, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the PF toggle button
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

/*** ---------------------------------------- Adding a root PF ----------------------------------------------------- ***/
//wait for element - add button, to be clickable
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), 300, 
    FailureHandling.CONTINUE_ON_FAILURE)

//Click on the add PF button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Add Root Node button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add Root PF Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//fill the create new PF pop-up
CustomKeywords.'com.amaze.utilities.commonUtilities.createPF'(pfID, pfTitle, attributeName)

/*** --------------------------------------- Adding Child PF ------------------------------------------------------  ***/
WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//finding the Root PF
WebDriver driver = DriverFactory.getWebDriver()

WebElement productFamilyGrid = driver.findElement(By.id('proFamGrid'))

List<WebElement> pfList = productFamilyGrid.findElements(By.tagName('span'))

for (WebElement list : pfList) {
    if (list.getText().equals(GlobalVariable.pfID_global)) {
        println('Selected value: ' + list)

        list.click()

        //wait for element - add button, to be clickable
        WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), 
            300)

        //Click on the add PF button
        WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'))

        WebUI.delay(1)

        //Select the add child PF node button
        WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF child Node button'))

        WebUI.delay(2)

        CustomKeywords.'com.amaze.utilities.commonUtilities.createPF'(childPF, pfTitle, attributeName)

        break
    }
}

/***  ---------------------------------Try to delete a root PF----------------------------------- ***/
WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//refresh the PF view by clicking on the selected taxonomy node again
CustomKeywords.'com.amaze.utilities.commonUtilities.refreshNode'(node)

//Select the root PF node that we had just created
driver.findElement(By.xpath(('//td[@title=\'' + GlobalVariable.pfID_global) + '\']')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Validate the visibility of the delete button
Boolean deleteButtonState = driver.findElement(By.xpath('//button[@ng-reflect-message=\'Delete Product Family\']')).isEnabled()

//Creating an object for soft assert
SoftAssert sa = new SoftAssert()

//Validating if the delete button is enabled.
sa.assertFalse(deleteButtonState)

/*** ------------------------------Delete PF/Data cleanup---------------------------------------  ***/
WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Expand Pf node
driver.findElement(By.xpath(('//td[@title=\'' + GlobalVariable.pfID_global) + '\']/div/div')).click()

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the child PF node
driver.findElement(By.xpath(('//td[@title=\'' + GlobalVariable.cPfID_global) + '\']/span/span')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Deleting the child node
//Click on the Delete DA button
driver.findElement(By.xpath('//button[@ng-reflect-message=\'Delete Product Family\']')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//click on the delete confirmation button
driver.findElement(By.xpath('//span[contains(text(), \'Yes\')]/parent::button')).click()

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//Delete the parent node
CustomKeywords.'com.amaze.utilities.commonUtilities.refreshNode'(node)

WebUI.delay(10)

WebUI.waitForElementPresent(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), 300)

//Select the parent PF node
driver.findElement(By.xpath(('//td[@title=\'' + GlobalVariable.pfID_global) + '\']/span/span')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Delete DA button
driver.findElement(By.xpath('//button[@ng-reflect-message=\'Delete Product Family\']')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//click on the delete confirmation button
driver.findElement(By.xpath('//span[contains(text(), \'Yes\')]/parent::button')).click()

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//navigate back to the catalog listing page
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

//Validate softAssert result
sa.assertAll()

