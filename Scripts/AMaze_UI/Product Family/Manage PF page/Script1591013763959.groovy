import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
// selenium webdriver class
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.Keys as Keys

//Login
/*WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
            1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
            'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.CONTINUE_ON_FAILURE)
*/
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//wait for PF toogle button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), 30, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

//Click on the PF toggle button
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

//wait for Manage PF button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Product Family/Manage PF Button'), 30, FailureHandling.STOP_ON_FAILURE)

/*** -------------------------------------------Add PF Node------------------------------------------------------------ ***/
//Click on add PF button
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Manage PF Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

//Click on the Add PF button
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Manage PF/Add PF button'), FailureHandling.STOP_ON_FAILURE)

//Click on the Add root node button
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Manage PF/Add Root Node'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

//Adding the PF Node
CustomKeywords.'com.amaze.utilities.commonUtilities.createPF'(pfID, pfTitle)

/*** ----------------------------------------------- Add Child node ------------------------------------------------ ***/
//Finding the currently added node
WebElement pfList = driver.findElement(By.xpath('//h4[text()=\'Product Families\']/parent::div/parent::div/following-sibling::div[2]//div[@id=\'amaze-tree-wrapper\']/div/ul[contains(@id, \'ft-id\')]'))

List<WebElement> pfLiList = pfList.findElements(By.tagName('li'))

for (WebElement pf : pfLiList) {
    if (pf.getText().contains(pfID)) {
        pf.click()

        break
    } else {
        println('Root node not found!')
    }
}

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Add PF button'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Manage PF/Add Child Node'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.createPF'(pfChildID, pfChildTitle)

/*** ----------------------------------------------- Add Attribute ------------------------------------------------ ***/
/*WebElement pfList1 = driver.findElement(By.xpath('//h4[text()=\'Product Families\']/parent::div/parent::div/following-sibling::div[2]//div[@id=\'amaze-tree-wrapper\']/div/ul[contains(@id, \'ft-id\')]'))

List<WebElement> pfLiList1 = pfList1.findElements(By.tagName('li'))

//selecting the node from the PF tree
for (WebElement pfAttr : pfLiList1) {
	println('GlobalVariable.pfID_global: '+ GlobalVariable.pfID_global)
	String newPFID =  GlobalVariable.pfID_global
    if (pfAttr.getText().contains(newPFID)) {
        List<WebElement> child = pfAttr.findElements(By.tagName('span'))

        for (WebElement spanChild : child) {
            if (spanChild.getText().contains('add_circle_outline')) {
                spanChild.click()
            }
        }
        
		WebUI.delay(8)
		
		println('GlobalVariable.cPfID_global '+ GlobalVariable.cPfID_global)
		//click on the child PF node
		driver.findElement(By.xpath("//span[@chipnodeid='"+GlobalVariable.cPfID_global+"']")).click()
		
        List<WebElement> childNode = pfList1.findElements(By.tagName('span'))

        for (WebElement liChild : childNode) {
            if (liChild.getAttribute("chipnodeid").contains(pfChildID)) {
                liChild.click()
				WebUI.delay(2)
            }
        }
    }
    
    break
}*/

//Expand the parent PF node
driver.findElement(By.xpath("//span[contains(text(),'"+GlobalVariable.pfID_global+"')]/parent::span/preceding-sibling::span[contains(text(),'add_circle_outline')]")).click()

WebUI.delay(2)

//Click on the child PF node
driver.findElement(By.xpath("//span[@chipnodeid='"+GlobalVariable.cPfID_global+"']")).click()

WebUI.delay(5)

//Click on the Add attribute button
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/Manage PF/Add Attribute Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

//Select an attribute from the attribute list
WebElement attrTable = driver.findElement(By.xpath('//table[@id=\'attrEntityAddMiniGrid\']'))

List<WebElement> tableData = attrTable.findElements(By.tagName('td'))

for (WebElement td : tableData) {
    if (td.getText().contains(newAttr)) {
        td.click()

        break
    }
}

//Click on the Add Attribute submit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Attribute Selection Submit'), 
    FailureHandling.STOP_ON_FAILURE)

println(('Attribute ' + newAttr) + ' has been added to the PF')

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

/*** ----------------------------------------------- Delete Attribute ------------------------------------------------- ***/
//Open the added attr to add value
Actions action = new Actions(driver)

WebElement addedAttr = driver.findElement(By.xpath('//*[@id="attrMiniGrid"]/div[1]/div/div/div/table/tbody/tr/td[3]'))

action.doubleClick(addedAttr).perform()

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.doubleClick(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF/Add Attribute Values'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF/Edit Attr Value TextArea'), '123', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF/Edit Attr Value TextArea'), Keys.chord(
        Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Product Family/Add PF/Edit Attr Value TextArea'), Keys.chord(Keys.ESCAPE), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

//Select an attribute to delete
driver.findElement(By.xpath('//*[@id="attrMiniGrid"]/div[1]/div/div/div/table/tbody/tr/td[3]')).click()

WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Delete Attribute Button'), 
    30, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Delete Attribute Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

/*** ----------------------------------------------- Add SKU ------------------------------------------------- ***/
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Add SKU button'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

//Selecting a SKU from the SKU table
WebElement skuTable = driver.findElement(By.xpath('//table[@id="skuEntityAddMiniGrid"]'))

List<WebElement> skuId = skuTable.findElements(By.tagName('td'))

for (WebElement sku : skuId) {
    if (sku.getAttribute("title").equals(skuID)) {
        sku.click()

        println(('SKU with SKU ID ' + skuID) + ' is clicked.')
    }
}

//clicking on the attach SKU button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Attach SKU button'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

/*** ----------------------------------------------- Delete SKU ------------------------------------------------- ***/
//Select a SKU to delete
driver.findElement(By.xpath('//*[@id=\'pfMiniGrid\']/div[4]/div/div/div/table/tbody/tr/td')).click()

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Delete SKU button'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Manage PF/Close Manage PF button'), FailureHandling.STOP_ON_FAILURE)

/*** ----------------------------------------------- Navigate to Catalog Listing page ------------------------------------------------- ***/
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

