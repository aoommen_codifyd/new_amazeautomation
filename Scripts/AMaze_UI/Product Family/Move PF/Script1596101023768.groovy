import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

//login using the username and password
not_run: CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : 'dev001', ('username') : 'allwyn'
        , ('password') : 'allwyn1234', ('gmailID') : 'aoommen@bluemeteor.com', ('gmailPassword') : 'Ao1234!@#$'], FailureHandling.STOP_ON_FAILURE)

//Navigate to the taxonomy and a taxonomy node.
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, node)

//wait for PF toogle button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), 30, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the PF toggle button
WebUI.click(findTestObject('AmazeTestObjects_UI/Product Family/PF Tab link'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

/*** ---------------------------------------- Adding a root PF ----------------------------------------------------- ***/
//wait for element - add button, to be clickable
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), 30, 
    FailureHandling.CONTINUE_ON_FAILURE)

//Click on the add PF button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Add Root Node button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add Root PF Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//fill the create new PF pop-up
CustomKeywords.'com.amaze.utilities.commonUtilities.createPF'(pfID, pfTitle)

/*** --------------------------------------- Adding Child PF ------------------------------------------------------  ***/
WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//finding the Root PF
WebDriver driver = DriverFactory.getWebDriver()

WebElement productFamilyGrid = driver.findElement(By.id('proFamGrid'))

List<WebElement> pfList = productFamilyGrid.findElements(By.tagName('span'))

for (WebElement list : pfList) {
    if (list.getText().equals(GlobalVariable.pfID_global)) {
        println('Selected value: ' + list)

        list.click()

        //wait for element - add button, to be clickable
        WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'), 
            30)

        //Click on the add PF button
        WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF Button'))

        WebUI.delay(1)

        //Select the add child PF node button
        WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Add PF child Node button'))

        WebUI.delay(2)

        CustomKeywords.'com.amaze.utilities.commonUtilities.createPF'(childPF, pfTitle)

        break
    }
}

/*** ------------------------------ Move node as root node ---------------------------------------  ***/
//Expand Pf node
driver.findElement(By.xpath(('//td[@title=\'' + GlobalVariable.pfID_global) + '\']/div/div')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the child PF node
driver.findElement(By.xpath(('//td[@title=\'' + GlobalVariable.cPfID_global) + '\']/span/span')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Wait for the move button
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Move PF/Move PF button'), 
    30, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the move PF button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Move PF/Move PF button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Select move as root node radio button
driver.findElement(By.xpath('//div[contains(text(), \'Move as top level PF\')]')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//click on the next button
driver.findElement(By.xpath('//span[contains(text(), \'Next\')]/parent::button')).click()

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Product Family/Move PF/Scroll to Move PF Button'), 20, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the move PF button
driver.findElement(By.xpath('//span[contains(text(), \'Move PF\')]/parent::button')).click()

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

/*** ---------------------------------------Move Under existing PF------------------------------------------  ***/
//Select the root node
driver.findElement(By.xpath(('//td[@title=\'' + GlobalVariable.cPfID_global) + '\']/span/span')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the move PF button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Product Family/Move PF/Move PF button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//click on the next button
driver.findElement(By.xpath('//span[contains(text(), \'Next\')]/parent::button')).click()

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//select the parent PF node
driver.findElement(By.xpath(('/html/body/div[3]/div[2]/div/mat-dialog-container/amaze-dialog/mat-dialog-content/app-move-productfamily-popup/div/mat-horizontal-stepper/div[2]/div[2]/div[2]/div/amaze-fancy-tree/div/div/div/div/ul/li//span[@title=\'' + 
        GlobalVariable.pfID_global) + '\']')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the next button
driver.findElement(By.xpath('/html/body/div[3]/div[2]/div/mat-dialog-container/amaze-dialog/mat-dialog-content/app-move-productfamily-popup/div/mat-horizontal-stepper/div[2]/div[2]/div[3]/div/button[2]')).click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Scroll down to button
WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Product Family/Move PF/Scroll to Move PF Button'), 20, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the move PF button
driver.findElement(By.xpath('//span[contains(text(), \'Move PF\')]/parent::button')).click()

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

