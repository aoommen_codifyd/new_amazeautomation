import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.google.common.collect.FilteredEntryMultimap.Keys as Keys
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.Keys as Keys

//Login
CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

//Navigate to catalog
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//Add SKU
String returnedSKUId = CustomKeywords.'com.amaze.utilities.commonUtilities.addSKU'(skuId, title, attrName, attrValue)

//check for SKU validation popup
WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'),
	300, FailureHandling.OPTIONAL)

WebUI.delay(2, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), FailureHandling.OPTIONAL)

WebUI.delay(3)

//Click on the filter button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU filter'))

WebUI.delay(2)

//check SKU ID checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU ID Filter Checkbox'))

WebUI.delay(2)

//Send SKU ID
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Filter Input textbox'), returnedSKUId)

WebUI.delay(1)

//Click on the Apply filters button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Apply Filter button'))

WebUI.delay(3)

//Select the checkbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU selection checkbox'))

WebUI.delay(2)

//Click on the move SKU button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Move SKU/Move SKU button'))

WebUI.delay(10)

//Click on the taxonomy search
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Move SKU/Taxonomy Search Box'))

//send new taxonomy node name
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Move SKU/Taxonomy Search Box'), newNode)

WebUI.delay(2)

WebDriver driver = DriverFactory.getWebDriver()

//Select the node from the dropdown
driver.findElement(By.xpath("//mat-option[@ng-reflect-value='"+newNode+"']")).click()

WebUI.delay(2)

//Click on the Move button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Move SKU/Move Submit Button'))

WebUI.delay(8)

/*** Data cleanup ***/
//Click on the global search text box
WebUI.click(findTestObject('AmazeTestObjects_UI/SKU/Global Search/Global Search Textbox'))

//Enter SKU ID in the global search textbox
WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/SKU/Global Search/Global Search Textbox'),returnedSKUId)

//Click on the glkobal search icon button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Global Search/Global Search Icon Button'))

WebUI.delay(8)

//click on the searched SKU card
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Global Search/SKU Search Card'))

WebUI.delay(5)

//Click on the SKU tilte to select the SKU
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU selection checkbox'))

WebUI.delay(2)

//Click on the delete button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Delete Button'))

//Click on the SKU delete confirmation
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Delete Confirmation'))

WebUI.delay(5)

/*** Logout ***/
CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()



