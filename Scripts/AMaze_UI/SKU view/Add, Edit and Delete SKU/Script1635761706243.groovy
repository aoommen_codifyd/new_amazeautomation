import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.google.common.collect.FilteredEntryMultimap.Keys as Keys
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.Keys as Keys

//Login
CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

//Navigate to catalog
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

/*** Add SKU ***/
WebDriver driver = DriverFactory.getWebDriver()

//Click on the add SKU button
WebElement addSkuButton = driver.findElement(By.xpath('//button[@mattooltip=\'Add SKU\']'))

addSkuButton.click()

WebUI.delay(3, FailureHandling.OPTIONAL)

//Click on the add SKU expansion panel
driver.findElement(By.xpath('//mat-expansion-panel[@id=\'add-sku-0\']')).click()

WebUI.delay(2)

//Click on the SKU item Id text
WebElement skuItemId = driver.findElement(By.xpath('//label[text() = \'SKU Item ID\']/following-sibling::input'))

skuItemId.click()

WebUI.delay(1)

String newSKUId = skuId + new Date().getTime()

//Set SKU item id
skuItemId.sendKeys(newSKUId)

//Click on the SKU title input
WebElement skuTitle = driver.findElement(By.xpath('//input[contains(@placeholder,\'SKU Title\')]'))

//if SKU title is enabled
if (skuTitle.isEnabled()) {
    skuTitle.click()

    WebUI.delay(1)

    //Set skuTitle
    skuTitle.sendKeys(title + new Date().getTime())
}

WebUI.delay(1)

/*** ------------------------------ Add attribute manually-------------------------------------------------- ***/
WebUI.delay(2)

//Click on the attriburte name input box
WebElement attrNameInput = driver.findElement(By.xpath('//input[contains(@placeholder,\'Type Attribute Name\')]'))

attrNameInput.click()

//Set attrName in the attr name input
attrNameInput.sendKeys(attrName)

WebUI.delay(5)

WebElement attrList = driver.findElement(By.xpath('//div[@role=\'listbox\']'))

List<WebElement> listAttr = attrList.findElements(By.tagName('mat-option'))

for (WebElement iList : listAttr) {
    if (iList.getAttribute('title').equals(attrName)) {
        iList.click()

        break
    }
}

WebUI.delay(3)

//double click to add attribute value
WebUI.doubleClick(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Attribute value double-click'))

WebUI.delay(3)

//click on the value text field
WebUI.doubleClick(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Attribute value field'))

//send value to the textarea
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Add Value text area'), attrValue)

//click on the enter button
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Add Value text area'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

//Click on the Add button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Attribute Value Add Button'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Add SKU button'))

WebUI.delay(8)

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), 
    300, FailureHandling.OPTIONAL)

WebUI.delay(2, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), FailureHandling.OPTIONAL)

WebUI.delay(3)

//Switch to Grid view
//WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Switch to Grid View'))

WebUI.delay(3)

/*** Edit SKU ***/

//Click on the filter button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU filter'))

WebUI.delay(2)

//check SKU ID checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU ID Filter Checkbox'))

WebUI.delay(2)

//Send SKU ID
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Filter Input textbox'), newSKUId)

WebUI.delay(1)

//Click on the Apply filters button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Apply Filter button'))

WebUI.delay(5)

//Select the checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU selection checkbox'))

WebUI.delay(2)

//Click on the SKU edit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Edit Button'))

WebUI.delay(1)

//Click on the edit SKU option
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Edit SKUs option'))

WebUI.delay(4)

//Click on the edit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/Edit SKU Submit Button'))

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'),
	300, FailureHandling.OPTIONAL)

WebUI.delay(2, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), FailureHandling.OPTIONAL)

WebUI.delay(3)

/*** Data cleanup ***/
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Delete Button'))

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Delete Confirmation'))

WebUI.delay(5)

/*** Logout ***/
CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

