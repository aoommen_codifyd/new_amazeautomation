import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Login
CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

//Navigate to catalog
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//Add SKU
String returnedSKUId = CustomKeywords.'com.amaze.utilities.commonUtilities.addSKU'(skuId, title, attrName, attrValue)

//check for SKU validation popup
//CustomKeywords.'com.amaze.utilities.commonUtilities.skuValidationFailurePopup'()

//Click on the filter button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU filter'))

WebUI.delay(2)

//check SKU ID checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU ID Filter Checkbox'))

WebUI.delay(2)

//Send SKU ID
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Filter Input textbox'), returnedSKUId)

WebUI.delay(1)

//Click on the Apply filters button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Apply Filter button'))

WebUI.delay(5)

//Select the checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU selection checkbox'))

WebUI.delay(2)

//Click on the DA button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/DA button'))

WebUI.delay(1)

//Click on the DA link button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Link DA button'))

WebUI.delay(2)

//click on the asset name textbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Asset Name Textbox'))

//Send DA search keys
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Asset Name Textbox'), daName)

WebUI.delay(3)

//Select the DA to link from the search result
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Asset Selection Checkbox'))

WebUI.delay(2)

//Click on the next button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Next button'))

WebUI.delay(2)

//Click on the submit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Link DA Submit Button'))

WebUI.delay(8)

/*** Detach the DA from the SKU ***/

//Click on the DA button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/DA button'))

WebUI.delay(1)

//Click on the DA delink button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/DA Delink button'))

WebUI.delay(5)

//Select all checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Select All Checkbox Delink'))

WebUI.delay(1)

//Click on the submit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Link Delink DA/Delink Submit button'))

WebUI.delay(5)

//logout
CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()