import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver



//Login
CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

//Navigate to catalog
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//Add SKU
String returnedSKUId = CustomKeywords.'com.amaze.utilities.commonUtilities.addSKU'(skuId, title, attrName, attrValue)

//check for SKU validation popup
//CustomKeywords.'com.amaze.utilities.commonUtilities.skuValidationFailurePopup'()

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'),
	300, FailureHandling.OPTIONAL)

WebUI.delay(2, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Validation Cancel button'), FailureHandling.OPTIONAL)

WebUI.delay(3)

//Click on the filter button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU filter'))

WebUI.delay(2)

//check SKU ID checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU ID Filter Checkbox'))

WebUI.delay(2)

//Send SKU ID
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Filter Input textbox'), returnedSKUId)

WebUI.delay(1)

//Click on the Apply filters button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Apply Filter button'))

WebUI.delay(5)

//Select the checkbox
WebUI.check(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU selection checkbox'))

WebUI.delay(2)

/*** Add SKU cross-listing ***/

//Click on the SKU crosslisting button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/SKU crosslist button'))

WebUI.delay(1)

//Click on the Add SKU cross listing button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/Add Crosslisting Button'))

WebUI.delay(8)

//Click on the Node search on crosslisting popup.
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/Node Search for Crosslisting'))

//Send node name to node search
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/Node Search for Crosslisting'), nodeName)

WebUI.delay(4)

//Click on the search node
WebDriver driver = DriverFactory.getWebDriver();
driver.findElement(By.xpath("//mat-option[@ng-reflect-value='"+nodeName+"']")).click()

WebUI.delay(2)

//Click on the crosslist SKU button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/Crosslist SKU Submit Button'))

WebUI.delay(8)

/*** Remove SKU crosslisting ***/

//click on the crosslisting button again.
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/SKU crosslist button'))

//Click on the remove cross listing button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/Remove Crosslisting Button'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/SKU Crosslisting/Remove Crosslisting Confirmation button'))

WebUI.delay(8)

/*** Delete SKU ***/

//Click on the delete button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Delete Button'))

//Click on the SKU delete confirmation
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/SKU/Add SKU/SKU Delete Confirmation'))

WebUI.delay(5)

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()