import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass as CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.testng.Assert as Assert
import org.testng.annotations.Test as Test
import org.testng.asserts.SoftAssert as SoftAssert

/*//login
CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)
*/
//login using gmail.
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

//navigate to catalog and the node
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

ArrayList schemaDetails = CustomKeywords.'com.amaze.utilities.commonUtilities.addSchema'(attrName, custID, schemaDesc, navigationOrder, lovValue)

String attributeName = schemaDetails[0]
/*** ---------------------------------------------Add Schema to a PF------------------------------------------------ ***/
WebDriver driver = DriverFactory.getWebDriver()
//Click on the attribute Name searchbox
driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).click()

//set the attribute name in the above selected textbox
driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).sendKeys(attributeName)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

//select the searched item
driver.findElement(By.xpath(('//td[contains(text(),\'' + attributeName) + '\')]/preceding-sibling::td/input[@role=\'checkbox\']')).click()

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the link to PF button
driver.findElement(By.xpath("//button[contains(@mattooltip,'Add/Remove Attribute(s) To Product Family')]")).click()

WebUI.delay(2)

//click on Add Attr to PF button
driver.findElement(By.xpath("//span[contains(text(),'Add Attribute(s) To PF')]/parent::button")).click()

WebUI.delay(5)

//Select the PF
driver.findElement(By.xpath("//span[contains(@title, 'PF001')]")).click()

//Wait for the next button
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema to PF/Next Button'), 
    3000)

//Click on the next button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema to PF/Next Button'))

WebUI.delay(2)

//Click on the value textbox
WebElement attrVal = driver.findElement(By.xpath('//input[@placeholder=\'Value\']'))

attrVal.click()

attrVal.clear()

//Send Value to attr value textbox
attrVal.sendKeys('65')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema to PF/Next 2 button'))

WebUI.delay(3)

//Click on the Add attr button
driver.findElement(By.xpath("//span[contains(text(), 'Add Attribute')]/parent::button")).click()

WebUI.delay(10)

/*** ---------------------------------------------Remove Attribute from PF------------------------------------------ ***/
//Click on the Add Attr to PF button
driver.findElement(By.xpath('//button[contains(@mattooltip,\'Add/Remove Attribute(s) To Product Family\')]')).click()

WebUI.delay(2)

//Click on Remove Attr from PF button
driver.findElement(By.xpath('//span[contains(text(), \'Remove Attribute(s) From PF\')]//parent::button')).click()

WebUI.delay(5)

//Click on the confirm removal button
driver.findElement(By.xpath('//span[contains(text(), \'Remove Attribute\')]//parent::button')).click()

WebUI.delay(15)

/*** ---------------------------------------------Delete Schema Attr/Data Cleanup----------------------------------- ***/
//DeleteSchema
CustomKeywords.'com.amaze.utilities.commonUtilities.deleteSchema'(attributeName)

//Navigate back to the catalo listing page
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

