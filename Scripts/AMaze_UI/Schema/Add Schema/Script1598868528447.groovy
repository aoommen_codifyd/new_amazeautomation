import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass as CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.testng.Assert as Assert
import org.testng.annotations.Test as Test
import org.testng.asserts.SoftAssert as SoftAssert

//login
//CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)

//login using gmail.
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

//navigate to catalog and the node
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Add schema
ArrayList schemaDetails = CustomKeywords.'com.amaze.utilities.commonUtilities.addSchema'(attrName,custID, navigationOrder,schemaDesc, lovValue)

String attributeName = schemaDetails[0]
/*** ---------------------------------------------Delete Schema Attr/Data Cleanup----------------------------------- ***/

WebUI.delay(10)
//Click on the attribute Name searchbox
driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).click()

//set the attribute name in the above selected textbox
driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).sendKeys(attributeName)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

//select the searched item
driver.findElement(By.xpath(('//td[contains(text(),\'' + attributeName) + '\')]/preceding-sibling::td/input[@role=\'checkbox\']')).click()

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the delete button
driver.findElement(By.xpath("//button[contains(@mattooltip,'Delete Schema(s)')]")).click()

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

//Confirm Schema attribute deletion
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Confirm Schema Submit'))

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

//Data assertion
//boolean schemaDeleted = CustomKeywords.'com.amaze.utilities.Assertions.schemaAssertion'(attributeName)
//
//println('schemaDeleted - ' + schemaDeleted)
//sa.assertFalse(schemaDeleted)
WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Navigate back to the catalo listing page
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

