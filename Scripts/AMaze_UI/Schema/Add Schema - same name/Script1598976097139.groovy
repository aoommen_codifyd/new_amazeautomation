import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass as CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.testng.Assert as Assert
import org.testng.annotations.Test as Test
import org.testng.asserts.SoftAssert as SoftAssert

/*//login
CustomKeywords.'com.amaze.utilities.commonUtilities.login'(company, username, password)
*/
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

//navigate to catalog and the node
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//Add Schema
ArrayList schemaDetails = CustomKeywords.'com.amaze.utilities.commonUtilities.addSchema'(attrName, custID, navigationOrder, schemaDesc, lovValue)

String attributeName = schemaDetails[0]
String customerID = schemaDetails[1]

/*** ---------------------------------------------Add anothe schema with same name---------------------------------- ***/
//Add Another schema attribute with the same name
//Click on the Add schema button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Attribute name textbox
WebUI.waitForElementPresent(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'),
		200, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'), FailureHandling.CONTINUE_ON_FAILURE)

//Set text on attribute name textbox
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Attribute Name Textbox'), attributeName)

WebUI.delay(2)

//Click on the Customer id input
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Customer ID Input'))

//Set Customer ID
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Customer ID Input'), customerID)

//click on add schema submit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Add Schema Submit button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

//Validate if the user is able to add the Schema attr
WebElement errorPopup = driver.findElement(By.xpath('//button[contains(text(),\'OK\')]'))

SoftAssert sa = new SoftAssert()

sa.assertNotNull(errorPopup)

WebUI.delay(2)

//click on the OK button
errorPopup.click()

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Add Schema Cancel Button'), 
    20)

WebUI.delay(2)

//Click on the cancel button
driver.findElement(By.xpath('//span[contains(text(),\'Cancel\')]')).click()

WebUI.delay(2)

/*** ---------------------------------------------Delete Schema Attr/Data Cleanup----------------------------------- ***/
//Click on the attribute Name searchbox
driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).click()

//set the attribute name in the above selected textbox
driver.findElement(By.xpath('//input[@id=\'gs_attributeName\']')).sendKeys(attributeName)

WebUI.delay(5)

//select the searched item
driver.findElement(By.xpath(('//td[contains(text(),\'' + attributeName) + '\')]/preceding-sibling::td/input[@role=\'checkbox\']')).click()

WebUI.delay(2)

//Click on the delete button
driver.findElement(By.xpath('//button[@mattooltip=\'Delete Schema(s)\']')).click()

WebUI.delay(3)

//Confirm Schema attribute deletion
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Schema/Add Schema Attr/Confirm Schema Submit'))
WebUI.delay(10)

//Navigate back to the catalo listing page
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

sa.assertAll()

