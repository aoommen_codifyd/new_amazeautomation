import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import com.kms.katalon.core.testobject.TestObject
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/*//Login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
            1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
            'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.CONTINUE_ON_FAILURE)
*/

//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
		, ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)


CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//Wait for Add Attribute button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add Attribute button'), 300, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the attribute metatag tab
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Meta Tag/Attribute Meta Tag Tab'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the add new attr meta tag attr button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/New Attr Meta tag'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//click on attr meta tag name field
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr meta tag name field'), FailureHandling.CONTINUE_ON_FAILURE)

String attrMetaTag = attrMName + new Date().getTime()

//set attr meta tag name
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr meta tag name field'), attrMetaTag,
	FailureHandling.CONTINUE_ON_FAILURE)

//Click on attr meta tag description textbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'),
	FailureHandling.CONTINUE_ON_FAILURE)

//Send attr metatag description
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'),
	attrMetaTagDesc, FailureHandling.CONTINUE_ON_FAILURE)

//Select metatag relevance dropdown
WebUI.selectOptionByLabel(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/MetaTag Relevance Dropdown'), metatagRelevance, false)

//Click on the Add meta tag button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Add Meta Tag submit button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

/*** ------------------------------------Add Attr meta data with same name --------------------------------------------- ***/

//Wait for Add attr metadata button
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Attribute Meta Tag/Attribute Meta Tag Tab'), 300)
//Click on the attribute metatag tab
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Meta Tag/Attribute Meta Tag Tab'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the add new attr meta tag attr button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/New Attr Meta tag'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//click on attr meta tag name field
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr meta tag name field'), FailureHandling.CONTINUE_ON_FAILURE)

//set attr meta tag name
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr meta tag name field'), attrMetaTag, 
    FailureHandling.CONTINUE_ON_FAILURE)

//Click on attr meta tag description textbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'), 
    FailureHandling.CONTINUE_ON_FAILURE)

//Send attr metatag description
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'), 
    attrMetaTagDesc, FailureHandling.CONTINUE_ON_FAILURE)

//Select metatag relevance dropdown
WebUI.selectOptionByLabel(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/MetaTag Relevance Dropdown'), metatagRelevance, false)

//Click on the Add meta tag button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Add Meta Tag submit button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

//Validate something went wrong banner
WebElement h2 = driver.findElement(By.xpath("//h2[contains(text(), 'Something Went Wrong')]"))
SoftAssert sa = new SoftAssert()
sa.assertNotNull(h2)

WebUI.delay(5)

//Click on the OK  button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Error Banner OK button'))

WebUI.delay(3)

//Click on the cancel button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Add meta tag popup Cancel button'))

WebUI.delay(2)


/*** ------------------------------------Delete Attr meta data---------------------------------------------------------- ***/
//Click on the attr meta tag searchbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr Meta Tag search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Send search text on the search box
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr Meta Tag search'), attrMetaTag, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the searched checkbox
WebElement checkBoxSelection = driver.findElement(By.xpath(('//td[@title=\'' + attrMetaTag) + '\']/preceding-sibling::td/input'))

checkBoxSelection.click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the delete button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Delete Meta tag button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the delete meta attribute button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Delete Confirmation button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

sa.assertAll()
