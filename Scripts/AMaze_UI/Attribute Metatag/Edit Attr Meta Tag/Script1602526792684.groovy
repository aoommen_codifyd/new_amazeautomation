import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import com.kms.katalon.core.testobject.TestObject
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

/*//Login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
            1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
            'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.CONTINUE_ON_FAILURE)
*/

//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//Wait for Add Attribute button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add Attribute button'), 30, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the attribute metatag tab
	WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Meta Tag/Attribute Meta Tag Tab'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	//Click on the add new attr meta tag attr button
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/New Attr Meta tag'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	//click on attr meta tag name field
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr meta tag name field'), FailureHandling.CONTINUE_ON_FAILURE)
	
	String attrMetaTag = attrMName + new Date().getTime()
	
	//set attr meta tag name
	WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr meta tag name field'), attrMetaTag,
		FailureHandling.CONTINUE_ON_FAILURE)
	
	//Click on attr meta tag description textbox
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'),
		FailureHandling.CONTINUE_ON_FAILURE)
	
	//Send attr metatag description
	WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'),
		attrMetaTagDesc, FailureHandling.CONTINUE_ON_FAILURE)
	
	//Select metatag relevance dropdown
	WebUI.selectOptionByLabel(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/MetaTag Relevance Dropdown'), metatagRelevance, false)
	
	//Click on the Add meta tag button
	WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Add Meta Tag submit button'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)


/*** ------------------------------------Edit Attr meta data---------------------------------------------------------- ***/
//Click on the attr meta tag searchbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr Meta Tag search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Send search text on the search box
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr Meta Tag search'), attrMetaTag, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the searched checkbox
WebDriver driver = DriverFactory.getWebDriver()

WebElement checkBoxSelection = driver.findElement(By.xpath(('//td[@title=\'' + attrMetaTag) + '\']/preceding-sibling::td/input'))

checkBoxSelection.click()

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the edit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Edit Attr Meta Tag button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the edit meta attr name textbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Edit meta attr name text'), FailureHandling.CONTINUE_ON_FAILURE)

//clear text
WebUI.clearText(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Edit meta attr name text'), FailureHandling.CONTINUE_ON_FAILURE)

String newTagName = editedMetaTag + new Date().getTime()

//Send new name
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Edit meta attr name text'), newTagName, 
    FailureHandling.CONTINUE_ON_FAILURE)

//click on description text area
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'), 
    FailureHandling.CONTINUE_ON_FAILURE)

//clear desc text area
WebUI.clearText(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'), 
    FailureHandling.CONTINUE_ON_FAILURE)

//send new desc
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attribute metatag description textbox'), 
    editedMetaTagDesc, FailureHandling.CONTINUE_ON_FAILURE)

//click on the update meta tag submit button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Update Meta tag Submit button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

/*** ------------------------------------Delete Attr meta data---------------------------------------------------------- ***/
//Click on the attr meta tag searchbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr Meta Tag search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Clear search box
WebUI.clearText(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr Meta Tag search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Send search text on the search box
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Attr Meta Tag search'), newTagName, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the searched checkbox

WebElement checkBoxSelection1 = driver.findElement(By.xpath("//td[@title='"+ newTagName +"']/preceding-sibling::td/input"))
checkBoxSelection1.click()

//Click on the delete button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Delete Meta tag button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the delete meta attribute button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Meta Tag/Delete Confirmation button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()


