import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

/*WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
        , ('password') : 'password'], FailureHandling.STOP_ON_FAILURE)
*/
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Amaze_API/Attributes Master List/Add Attribute'), [:], FailureHandling.STOP_ON_FAILURE)

//Navigate to attribute master list.
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//search attribute
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'))

WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), GlobalVariable.newAttr)

WebUI.delay(5)

//select teh attr
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Seletion Checkbox'))

//click on edot attr button
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Button'))

WebUI.delay(2)

//click on attr name input box
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/Attribute Name Textbox Edit'))

WebUI.clearText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/Attribute Name Textbox Edit'))

WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/Attribute Name Textbox Edit'), 
    GlobalVariable.newAttr + 'edited')

WebUI.delay(2)

WebUI.selectOptionByValue(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/Edit Datatype'), 
    dataType, false)

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/LoV TextBox Edit'))

WebUI.clearText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/LoV TextBox Edit'))

WebUI.setText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/LoV TextBox Edit'), lovValue)

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/Submit Edit Attribute'), 
    300)

WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Edit Attribute Popup/Submit Edit Attribute'))

//println('result - ' + editedLov)
WebUI.delay(5)

WebUI.callTestCase(findTestCase('Test/Navigate to Catalog Landing page'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

