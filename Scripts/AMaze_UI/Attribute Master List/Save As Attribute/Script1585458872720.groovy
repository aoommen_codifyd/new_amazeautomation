import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

/*//login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
        , ('password') : 'password'], FailureHandling.STOP_ON_FAILURE)
*/
//login using username and password
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

//Add Attribute API
WebUI.callTestCase(findTestCase('Amaze_API/Attributes Master List/Add Attribute'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

//wait for attr master list
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), 30)

//click on attr search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'))

//set search text
WebUI.setText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), GlobalVariable.newAttr)

WebUI.delay(5)

//check the selection checkbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/First Checkbox'))

//wait for save as attribute button.
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Save As Attribute/Save As Attribute Button'), 
    20)

//click on the save as attr button
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Save As Attribute/Save As Attribute Button'))

//wait for the attr name text box
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Attribute Master List/Save As Attribute/Attribute Name'), 
    20)

//change the attribute name and save as
WebUI.setText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Save As Attribute/Attribute Name'), GlobalVariable.newAttr + 
    '_saveAsTest')

WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Save As Attribute/Submit'))

WebUI.delay(5)

WebUI.callTestCase(findTestCase('Test/Navigate to Catalog Landing page'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

