import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import com.kms.katalon.core.testobject.TestObject
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

/*//Login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
            1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
            'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)
*/
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

WebUI.callTestCase(findTestCase('Amaze_API/Attributes Master List/Add Attribute'), [('catalogID') : GlobalVariable.catalog_Id], 
    FailureHandling.STOP_ON_FAILURE)

//try to add another attribute with the same name
//Wait for Add Attribute button
//Click on Add Attribute
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add Attribute button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

//Click on the Attr name textbox
 WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'))

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'), 300)

//clear attribute name textbox
WebUI.clearText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'))

	
attrName = GlobalVariable.newAttr

//Set attr name
WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'), attrName)

//set customer attr ID
WebElement custAttrIDInput = driver.findElement(By.xpath("//input[contains(@placeholder,'Type Customer Attribute Id')]"))
custAttrIDInput.click()
String custId = GlobalVariable.CustId
custAttrIDInput.sendKeys(custId)

WebUI.delay(2)

//Click on the submit button
driver.findElement(By.xpath("//span[contains(text(), 'Add Attribute')]/parent::span/parent::button")).click()

WebUI.delay(2)

//Click on OK button for banner
driver.findElement(By.xpath("//button[contains(text(), 'OK')]")).click()

WebUI.delay(1)

//Click on the cancel button
driver.findElement(By.xpath("//span[contains(text(), 'Cancel')]/parent::span//parent::button")).click()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

