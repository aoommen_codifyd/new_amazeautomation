import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import com.kms.katalon.core.testobject.TestObject
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.support.ui.Select;

/*WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
            1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
            'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)
*/

//Login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
	, ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add Attribute button'), 300)

WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add Attribute button'))

WebUI.delay(2)

//Add 1st Attribute
CustomKeywords.'com.amaze.utilities.commonUtilities.addAttributePopUp'(driver, attributeName, "Id"+new Date().getTime() , "TestDesc", dataType, isGlobal, multipleUom, canHaveLov, uomValue, lovValue)

GlobalVariable.attributeNames = attributeName

//Click on the Add more Attribute button
driver.findElement(By.xpath("//a[contains(@ng-reflect-message,'Add More Attributes(s)')]")).click()

WebUI.delay(2)

//Click & set text on 2nd attr name
WebElement attrTwo = driver.findElement(By.xpath("//input[@ng-reflect-name='attribute.attributeName-1']"))
attrTwo.click()
String attrName2 = attributeName2 + new Date().getTime()
attrTwo.sendKeys(attrName2)
GlobalVariable.attributeNames=attrName2

//Select the datatype
Select dataTypeTwo = new Select(driver.findElement(By.xpath("//select[@ng-reflect-name='attribute.dataType-1']")))
dataTypeTwo.selectByValue(dataType2)

//can have LoV
	if(canHaveLov.equalsIgnoreCase('Y')){
		driver.findElement(By.xpath("//input[contains(@id,'UAT-attribute-canhaveLOV1')]/following-sibling::span")).click()
		
		WebUI.delay(2)
		
		//Click on UoM div
		driver.findElement(By.xpath("//span[contains(text(),'Add UoM(s)')]/parent::div")).click()
		
		WebUI.delay(1)
		
		//Select the UoM
		WebElement uomInput = driver.findElement(By.xpath("//parent::div[@id='attributeUoms-1-0']/input[contains(@placeholder,'Type UoM')]"))
		uomInput.click()
		uomInput.sendKeys(uomValue)
		WebUI.delay(2)
		driver.findElement(By.xpath("//span[contains(text(), 'cm')]/parent::mat-option")).click()
		
		WebUI.delay(1)
		
		//Set LoV
		//click on the LoV div
		driver.findElement(By.xpath("//span[contains(text(),'Add LoV(s)')]/parent::div")).click()
		
		WebUI.delay(1)
		
		//click on the Lov value input box
		WebElement lovInput = driver.findElement(By.xpath("//input[contains(@id,'UAT-attribute-LOV-1')]"))
		lovInput.click()
		
		//set LoV value
		lovInput.sendKeys(lovValue2)
		
		//Select UoM for LoV values
		Select uomSelector = new Select(driver.findElement(By.xpath("//select[contains(@id,'UAT-attribute-selectUOM-1')]")))
		uomSelector.selectByVisibleText(uomValue)
	}

//Click on the submit button
driver.findElement(By.xpath("//span[contains(text(), 'Add Attribute')]/parent::span/parent::button")).click()

WebUI.delay(5)

//adding attribute name to a list
List attributeList = new ArrayList<String>()
attributeList.add(attributeName)
attributeList.add(attrName2)

//delete Attributes -- data clean up
for(String attrs: attributeList){
	
	WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), 300)
	
		WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'))
	
		WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), attrs)
	
		WebUI.delay(2)
	
		//delete
		WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/First Checkbox selection'))
	
		WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute Button'), 300)
	
		WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute Button'))
	
		WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/Delete Yes Button'),
			20)
	
		WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/Delete Yes Button'))
	
		println(('Attribute ' + attrs) + ' has been deleted.')
	
		WebUI.delay(10)
	
		WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search Cancel'))
	
		WebUI.delay(3)
}


CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

