import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import org.openqa.selenium.support.ui.Select;
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

def attrName

/*//Login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
            1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
            'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.CONTINUE_ON_FAILURE)
*/
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add Attribute button'), 300, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

def attributeName = (([attrNameString, attrNameDecimal, attrNameInt, attrNameDerived]) as String[])

def dataType = (([dTypeString, dTypeDecimal, dTypeInteger, dTypeDerived]) as String[])

def lovValue = (([lovValueString, lovValueDecimal, lovValueInteger, lovValueDerived]) as String[])

WebDriver driver = DriverFactory.getWebDriver()

for (int i = 0; i < 4; i++) {
    //Click on the add attribute button
    WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add Attribute button'))

	WebUI.delay(2)
	
    //Click on the Attr name textbox
    WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'))

    WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'), 300)

	//clear attribute name textbox
    WebUI.clearText(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'))

	
    attrName = ((attributeName[i]) + new Date().getTime())
	
	//Set attr name
	WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Add Attribute Name Textbox'), attrName)

    //set customer attr ID
	WebElement custAttrIDInput = driver.findElement(By.xpath("//input[contains(@placeholder,'Type Customer Attribute Id')]"))
	custAttrIDInput.click()
	String custId = custAttrId + new Date().getTime()
	custAttrIDInput.sendKeys(custId)
	
	//Set Attr description
	WebElement attrDesc = driver.findElement(By.xpath("//textarea[contains(@placeholder,'Type Attribute Description')]"))
	attrDesc.click()
	attrDesc.sendKeys(attrDescTest)
	
	//set datatype
	Select datatypedd = new Select(driver.findElement(By.xpath("//select[contains(@id, 'UAT-attribute-datatype-0')]")))
	datatypedd.selectByValue(dataType[i])
	
	//if derived attr then select the formula
	if(dataType[i].equalsIgnoreCase("DERIVED")){
		
		WebUI.delay(1)
		
		//Click formula input box
		driver.findElement(By.xpath("//input[contains(@placeholder, 'Select Formula')]")).click()
		
		WebUI.delay(2)
		
		//Select the formula
		driver.findElement(By.xpath("//td[contains(@title,'test22')]")).click()
		
		WebUI.delay(2)
		
		//click on the select formula button
		driver.findElement(By.xpath("//button[@id='UAT-attribute-select-formula-proceed']")).click()
		
		WebUI.delay(2)
	}
	
	//is Global?
	if (isGlobal.equalsIgnoreCase('Y')) {
		WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Add attribute popup/Is Global'))
	}

	//can have multiple UoMs
	if(multipleUom.equalsIgnoreCase('Y')){
		driver.findElement(By.xpath("//input[contains(@ng-reflect-name,'attribute.canHaveMultipleUom')]/following-sibling::span")).click()
	}
	
	//can have LoV
	if(canHaveLov.equalsIgnoreCase('Y')){
		driver.findElement(By.xpath("//input[contains(@id,'UAT-attribute-canhaveLOV')]/following-sibling::span")).click()
		
		WebUI.delay(2)
		
		//Click on UoM div
		driver.findElement(By.xpath("//span[contains(text(),'Add UoM(s)')]/parent::div")).click()
		
		WebUI.delay(2)
		
		//Select the UoM
		WebElement uomInput = driver.findElement(By.xpath("//input[contains(@placeholder,'Type UoM')]"))
		uomInput.click()
		uomInput.sendKeys(uomValue)
		WebUI.delay(5)
		driver.findElement(By.xpath("//span[contains(text(), 'Centimeter(cm)')]/parent::mat-option")).click()
		
		WebUI.delay(2)
		
		//Set LoV
		//click on the LoV div
		driver.findElement(By.xpath("//span[contains(text(),'Add LoV(s)')]/parent::div")).click()
		
		WebUI.delay(1)
		
		//click on the Lov value input box
		WebElement lovInput = driver.findElement(By.xpath("//input[contains(@id,'UAT-attribute-LOV')]"))
		lovInput.click()
		
		//set LoV value
		lovInput.sendKeys(lovValue[i])
		
		//Select UoM for LoV values
		Select uomSelector = new Select(driver.findElement(By.xpath("//select[contains(@id,'UAT-attribute-selectUOM')]")))
		uomSelector.selectByVisibleText(uomValue)
	}

	WebUI.delay(2)
	
	//Click on the submit button
	driver.findElement(By.xpath("//span[contains(text(), 'Add Attribute')]/parent::span/parent::button")).click()
	
	WebUI.delay(10)
	
    //clean up
    WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), 300)

    WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'))

    WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), attrName)

    WebUI.delay(15)

    //delete
    WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/First Checkbox selection'))

    WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute Button'), 300)

    WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute Button'))

    WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/Delete Yes Button'), 
        20)

    WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/Delete Yes Button'))

    println(('Attribute ' + attrName) + ' has been deleted.')

    WebUI.delay(10)

    WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search Cancel'))

    WebUI.delay(5)
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

