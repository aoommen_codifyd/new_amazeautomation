import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import com.paulhammant.ngwebdriver.ByAngular as ByAngular
import com.paulhammant.ngwebdriver.NgWebDriver as NgWebDriver
import org.openqa.selenium.WebElement as WebElement

//login
not_run: WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
        , ('password') : 'password'], FailureHandling.STOP_ON_FAILURE)


CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, nodeName)

WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/First Checkbox selection'), 
    30)

WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'))

WebUI.sendKeys(findTestObject('AmazeTestObjects_UI/Attribute Master List/Attribute Search'), attributeName)

WebUI.delay(2)

//delete
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/First Checkbox selection'))

WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute Button'), 30)

WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute Button'))

WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/Delete Yes Button'), 
    20)

WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master List/Delete Attribute/Delete Yes Button'))

println(('Attribute ' + nodeName) + ' has been deleted.')

WebUI.delay(5)

WebUI.callTestCase(findTestCase('Test/Navigate to Catalog Landing page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

not_run: WebUI.callTestCase(findTestCase('AMaze_UI/Logout_UI/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

