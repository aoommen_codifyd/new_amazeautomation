import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
//import com.kms.katalon.core.testobject.TestObject
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.ss.usermodel.ConditionType as ConditionType
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

def attrName

WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('URL') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

/*
//Login using username&password
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
            1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
            'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.CONTINUE_ON_FAILURE)
*/
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, taxNode)

WebUI.delay(2)

//Toggle to attribute group view
WebUI.click(findTestObject('AmazeTestObjects_UI/Attribute Master Group/Attribute Group Tab'))

WebUI.delay(2)

/***-----------------------------------------------Add Attr group------------------------------------------------------------------------------------------------***/
//Click on the add Attribute group button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Add Attr Group button'))

WebUI.delay(2)

//Click on attr group name textbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Attr group name textbox'))

WebUI.delay(2)

String attrGName = attrGroupName + new Date().getTime()

//send group name 
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Attr group name textbox'), attrGName)

//Click on the attr group name desc
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Attr Group name desc'))

//Send Attr group name desc
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Attr Group name desc'), attrGroupDesc)

//Click on the Add button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Add Attr Group Submit button'))

WebUI.delay(10)

/*** ---------------------------------------------Data cleanup/Delete the attr master group---------------------------------------------- ***/
//Select the above created attr group
WebDriver driver = DriverFactory.getWebDriver()

WebElement newGroup = driver.findElement(By.xpath(('//span[contains(text(),\'' + attrGName) + '\')]/parent::p/parent::mat-panel-title'))

newGroup.click()

WebUI.delay(2)

//Wait for activation of delete button
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Delete Attr Group button'), 
    10)

//Click on the delete button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Delete Attr Group button'))

WebUI.delay(2)

//Click on the delete confirmation button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Attribute Master Group/Delete Attr Group Master Submit button'))

WebUI.delay(10)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

