import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

//Wait for catalog search box
WebUI.waitForPageLoad(40)

//WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 30)
//Click on catalog search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

//Type catalog name in the searchbox.
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

WebUI.delay(2)

//Launch the catalog
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

WebUI.delay(60)

//Wait for taxonomy search box.
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'))

//set search text
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), nodeName)

WebUI.delay(5)

//Select the taxonomy search dropdown
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Taxonomy_dropdown'))

//wait for link-delink button
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link Delink DA button'), 20)

//click on the link delink button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link Delink DA button'))

WebUI.delay(2)

//click on the link button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link DA button'))

WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'), 30)

//click on the Asset name searchbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'))

//set search text
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'), assetName)

WebUI.delay(2)

//wait for selection checkbox
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Select DA Checkbox'), 20)

//select the selection checkbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Select DA Checkbox'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link DA Next Button'), 30)

//next button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link DA Next Button'))

WebUI.delay(2)

//Click on the submit DA link button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Submit Link DA'))

println(((('Digital Asset - ' + assetName) + ' has been attached to ') + nodeName) + ' Node.')

WebUI.delay(5)

//navigate to landing page
WebUI.callTestCase(findTestCase('Test/Navigate to Catalog Landing page'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

