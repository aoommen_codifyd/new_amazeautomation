import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalog'(catalog, nodeName)

//wait for link delink button to be active
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link Delink DA button'), 300)

//click on the link delink button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link Delink DA button'))

WebUI.delay(2)

//click on the link DA button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link DA button'))

//Wait for DA asset page
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'), 30)

//Click on the asset name search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'))

//search for the DA
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Asset Name Search box'), assetName)

WebUI.delay(2)

//wait for the DA element
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Select DA Checkbox'), 300)

//check on the DA checkbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Select DA Checkbox'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link DA Next Button'), 200)

//Click on the next button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link DA Next Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Submit Link DA'), 20)

//Click on the submit button.
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Submit Link DA'))

println(((('Digital Asset - ' + assetName) + ' has been attached to ') + nodeName) + ' Node.')

WebUI.delay(10)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link Delink DA button'), 300)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Link Delink DA button'))

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Delink DA button'))

WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Delink DA selection Checkbox'), 300)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Delink DA selection Checkbox'))

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Link DA/Delink DA Submit button'))

WebUI.delay(10)

//CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()
CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

