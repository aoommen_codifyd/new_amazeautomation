import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

/*//login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
        , ('password') : 'password'], FailureHandling.STOP_ON_FAILURE)*/
//login using gmail.
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

//Wait for catalog search box
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 5000)

//Click on catalog search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

//Type catalog name in the searchbox.
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

WebUI.delay(2)

//Launch the catalog
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

//WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 5000)

WebUI.delay(35)

//Click on the Add Attribute button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(2)

//Click on the Add root node button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Button'))

//wait for Add root node popup
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), 2000)

String newNodeName = nodeName + new Date().getTime()

GlobalVariable.newNodeName = newNodeName

//set text add new root node
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), newNodeName)

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'), 3000)

//Click on the Add button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'))

println(('Root node ' + newNodeName) + ' added.')

GlobalVariable.deleteNode = newNodeName

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Object Repository/AmazeTestObjects_UI/Catalog_UI/Catalog link'), 5000)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

//data clean up
WebUI.callTestCase(findTestCase('AMaze_UI/Taxonomy/Delete Node/Delete Node'), [('catalog') : findTestData('Amaze/Amaze_UI_Taxonomy/Delete Node').getValue(
            1, 1), ('nodeName') : GlobalVariable.deleteNode], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

