import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//login
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

//Wait for catalog search box
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

WebUI.delay(2)

//Click on catalog search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

//Type catalog name in the searchbox.
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

WebUI.delay(2)

//Launch the catalog
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 300)

WebUI.delay(2)

//Wait for Add Taxonomy
WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 300)

//Click on the Add Attribute button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(8)

//Click on the Add root node button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Button'))

//wait for Add root node popup
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), 200)

String newNodeName = rootNode + new Date().getTime()

println('node name' + newNodeName)

//set text add new root node
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), newNodeName)

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'), 200)

//Click on the Add button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'))

WebUI.delay(10)

//Wait for taxonomy search box.
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'))

//set search text
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), newNodeName)

WebUI.delay(5)

//Select the taxonomy search dropdown
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Taxonomy_dropdown'))

//wait for edit taxonomy button
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Edit Taxonomy/Edit Taxonomy Button'), 300)

//Click on the Add taxonomy button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(10)

//click on the Add bulk node before button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes After/Add Bulk Nodes After Button'))

//Wait for the popup to open
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes After/Bulk Node After textbox1'), 
    30)

String node1 = bulkNode1 + new Date().getTime()

println('First Node to be Added - ' + node1)

//set the name of the 1st node
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes Before/Bulk Node Before textbox1'), node1)

//click on the Add more node button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes After/Add More Nodes 1'))

WebUI.delay(2)

//Click on the next textbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes After/Bulk Node After Textbox2'))

String node2 = bulkNode2 + new Date().getTime()

println('Second Node to be added - ' + node2)

//set the name for the 2nd node
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes After/Bulk Node After Textbox2'), node2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes After/Submit Button'), 20)

//Click on the submit button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Nodes After/Submit Button'))

WebUI.delay(5)

//navigate to the landing page
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

println((((('Two nodes - ' + node1) + ' & ') + node2) + ' have been added before Node - ') + newNodeName)

WebUI.delay(5)

//Data clean up
/*
def delNodes = (([node1, node2, newNodeName]) as String[])

for (int i = 0; i < 3; i++) {
    GlobalVariable.deleteNode = (delNodes[i])

    WebUI.callTestCase(findTestCase('AMaze_UI/Taxonomy/Delete Node/Delete Node'), [('catalog') : findTestData('Amaze/Amaze_UI_Taxonomy/Delete Node').getValue(
                1, 1), ('nodeName') : GlobalVariable.deleteNode], FailureHandling.STOP_ON_FAILURE)
}
*/
CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

