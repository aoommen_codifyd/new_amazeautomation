import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

/*//Call login Test case
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
        , ('password') : 'password'], FailureHandling.STOP_ON_FAILURE)
*/
//Login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

//Wait for catalog search box
WebUI.waitForPageLoad(40)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

//Click on catalog search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

//Type catalog name in the searchbox.
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

WebUI.delay(2)

//Launch the catalog
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 5000)

//Click on the Add Attribute button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(2)

//Click on the Add root node button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Button'))

//wait for Add root node popup
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), 300)

String newNodeName = nodeName + new Date().getTime()

GlobalVariable.newNodeName = newNodeName

println('node name' + newNodeName)

//set text add new root node
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), newNodeName)

WebUI.delay(2)

//Click on the Add button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'))

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), 300)

WebUI.delay(10)

//Wait for taxonomy search box.
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'))

//set search text
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), newNodeName)

WebUI.delay(5)

//Select the taxonomy search dropdown
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Taxonomy_dropdown'))

//wait for edit taxonomy button
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Edit Taxonomy/Edit Taxonomy Button'), 300)

//Click on the Add taxonomy button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(5)

//Click on the Add Child node button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Child Node/Add Child node Button'))

//Wait for the popup to open
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Child Node/Add Child Node Textbox'), 200)

//Child node variable
String childNode = childNodeName + new Date().getTime()

//Set the Child node name
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Child Node/Add Child Node Textbox'), childNode)

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Child Node/Add child Submit button'), 30)

//Click on the submit button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Child Node/Add child Submit button'))

println((('Child Node ' + childNode) + ' added to root node ') + newNodeName)

WebUI.delay(5)

//Data cleanup
//Click on the delete node link
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Link'), 5000)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Link'))

WebUI.delay(2)

//Click on delete node hierarchy
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Hierarchy'))

WebUI.delay(2)

//delete node with SKUs option
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node and SKUs link'))

WebUI.delay(2)

//delete confirmation
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Confirm Deletion Button'))

println('data clean up finished')

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

