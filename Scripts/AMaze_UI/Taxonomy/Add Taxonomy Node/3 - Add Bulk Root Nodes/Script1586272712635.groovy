import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Calling login textox
/*WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
        , ('password') : 'password'], FailureHandling.STOP_ON_FAILURE)
*/
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

//Wait for catalog search box
WebUI.waitForPageLoad(40)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

//Click on catalog search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

//Type catalog name in the searchbox.
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

WebUI.delay(2)

//Launch the catalog
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

WebUI.delay(2)

//Wait for Add Taxonomy
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 300, FailureHandling.OPTIONAL)

//Click on the Add tax button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(2)

//Click on the Add bulk root nodes button.
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add Bulk Root Node Button'))

//Wait for the popup to open
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add Bulk Node Textbox 1'), 
    20)

String node1 = rootNode1 + new Date().getTime()

//Set text 1
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add Bulk Node Textbox 1'), node1)

//Click on Add more node '+' button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add More Node Button'))

//Click on teh next textbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add Bulk Node TextBox 2'))

String node2 = rootNode2 + new Date().getTime()

//set text in textbox 2
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add Bulk Node TextBox 2'), node2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add Bulk Root Node Submit Button'), 
    30)

//Click on the submit button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Bulk Root Nodes/Add Bulk Root Node Submit Button'))

println((('Bulk root nodes created - ' + node1) + ' & ') + node2)

WebUI.delay(10)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

def delNodes = (([node1, node2]) as String[])

for (int i = 0; i < 2; i++) {
    GlobalVariable.deleteNode = (delNodes[i])

    WebUI.callTestCase(findTestCase('AMaze_UI/Taxonomy/Delete Node/Delete Node'), [('catalog') : findTestData('Amaze/Amaze_UI_Taxonomy/Delete Node').getValue(
                1, 1), ('nodeName') : GlobalVariable.deleteNode], FailureHandling.CONTINUE_ON_FAILURE)
}

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

