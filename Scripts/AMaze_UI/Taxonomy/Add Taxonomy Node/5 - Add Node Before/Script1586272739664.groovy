import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//login
/*WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
        , ('password') : 'password'], FailureHandling.STOP_ON_FAILURE)
*/
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : GlobalVariable.company, ('gmailID') : GlobalVariable.gmailID
        , ('gmailPassword') : GlobalVariable.gmailPassword, ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

//Wait for catalog search box
WebUI.waitForPageLoad(40)

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

//Click on catalog search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

//Type catalog name in the searchbox.
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

WebUI.delay(2)

//Launch the catalog
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

//Wait for Add Taxonomy
WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 300, FailureHandling.OPTIONAL)

WebUI.delay(30)

//Click on the Add Taxonomy button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(2)

//Click on the Add root node button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Button'))

//wait for Add root node popup
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), 20)

String newNodeName = nodeName + new Date().getTime()

println('node name' + newNodeName)

//set text add new root node
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), newNodeName)

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'), 30)

//Click on the Add button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'))

WebUI.delay(10)

//Wait for taxonomy search box.
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'))

//set search text
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), newNodeName)

WebUI.delay(5)

//Select the taxonomy search dropdown
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Taxonomy_dropdown'))

//wait for edit taxonomy button
WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Edit Taxonomy/Edit Taxonomy Button'), 30)

//Click on the Add taxonomy button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(5)

//Click on the Add Node Before button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Node Before/Add Node Before Button'))

WebUI.delay(2)

String nodeBefore = newNodeBefore + new Date().getTime()

println('before node to be added - ' + nodeBefore)

//set the node name in the text box
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Node Before/Before Node Textbox'), nodeBefore)

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Node Before/Add Node Before Submit Button'), 30)

//click on the submit button
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Node Before/Add Node Before Submit Button'))

println((('A new node - ' + nodeBefore) + ' has been added before the node - ') + newNodeName)

WebUI.delay(5)

//Navigate to catalog landing page
CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

/*** ------------------------------------------Data Cleanup--------------------------------------------- ***/
def delNodes = (([nodeBefore, newNodeName]) as String[])

for (int i = 0; i < 2; i++) {
    GlobalVariable.deleteNode = (delNodes[i])

    WebUI.callTestCase(findTestCase('AMaze_UI/Taxonomy/Delete Node/Delete Node'), [('catalog') : findTestData('Amaze/Amaze_UI_Taxonomy/Delete Node').getValue(
                1, 1), ('nodeName') : GlobalVariable.deleteNode], FailureHandling.STOP_ON_FAILURE)
}

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()

