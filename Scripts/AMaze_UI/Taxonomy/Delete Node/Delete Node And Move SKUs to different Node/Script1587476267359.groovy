import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver

//Wait for catalog search box
/*WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(
 1, 1), ('username') : findTestData('Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(2, 1), ('password') : findTestData(
 'Amaze/Amaze_Login_Data/Login_UsernamePassword').getValue(3, 1), ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)
 */
//login using gmail
WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/testAmazeLogin_gmail'), [('company') : 'dev001', ('username') : 'allwyn'
	, ('password') : 'allwyn1234', ('gmailID') : GlobalVariable.gmailID, ('gmailPassword') : GlobalVariable.gmailPassword],
FailureHandling.STOP_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver();

WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 300)

WebUI.delay(10)

//Click on catalog searchbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

WebUI.delay(3)

//Set catalog name
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), catalog)

WebUI.delay(3)

//Click on the launch button
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'), 300)

WebUI.delay(2)

//click on the add taxonomy button.
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Taxonomy Button'))

WebUI.delay(1)

//click on add root node button
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Button'))

WebUI.delay(2)

//Click on the node name textbox
WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'))

String rootNode = nodeName + new Date().getTime()
//Send text 
WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Root Node Text Box'), rootNode)

//Click on the submit button
WebUI.delay(1)

WebUI.scrollToElement(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'), 300)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Add Taxonomy/Add Button'))

WebUI.delay(10)

//click on the taxonomy search button
WebUI.waitForElementVisible(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), 300, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'))

WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), rootNode)

WebUI.delay(5)

//click on the selection
driver.findElement(By.xpath("//mat-option[contains(@title, '"+rootNode+"')]")).click()

WebUI.delay(5)

//Click on  the delete button
WebUI.waitForElementClickable(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Link'),300)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Link'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Button'))

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node and Move SKUs to diff node link'))

WebUI.delay(10)

//select the node from the taxonomy tree

WebUI.waitForElementVisible(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Search Node for Deletion'), 300)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Search Node for Deletion'))

WebUI.sendKeys(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Search Node for Deletion'), newNode)

WebUI.delay(2)

driver.findElement(By.xpath("//mat-option[contains(@title, '"+newNode+"')]")).click()

WebUI.delay(2)

//click on the next button

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Next Button'), 300)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Next Button'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Confirm Deletion Button'), 300)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Confirm Deletion Button'))

WebUI.delay(10)

WebUI.waitForElementVisible(findTestObject('Object Repository/AmazeTestObjects_UI/Catalog_UI/Catalog link'), 300)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

CustomKeywords.'com.amaze.utilities.commonUtilities.logout'()




