import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

/*WebUI.callTestCase(findTestCase('AMaze_UI/Login_UI/Login_usernamePassword'), [('company') : 'dev001', ('username') : 'aoommen@codifyd.com'
 , ('password') : 'password', ('url') : GlobalVariable.URL], FailureHandling.STOP_ON_FAILURE)
 */
//Wait for catalog search box

WebUI.delay(5)


WebUI.waitForElementPresent(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'), 6000) //Click on catalog search box
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search'))

//Type catalog name in the searchbox.
WebUI.setText(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog_Search')
		, GlobalVariable.catalog)

WebUI.delay(5)

//Launch the catalog
WebUI.click(findTestObject('AmazeTestObjects_UI/Catalog_UI/Catalog Launch Button'))

WebUI.waitForElementVisible(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), 5000)

//Wait for Taxonomy searchbox
WebUI.delay(2)

//Click on the taxonomy searchbox
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'))

//set node name
WebUI.setText(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/TaxonomySearchBox'), GlobalVariable.deleteNode)

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Taxonomy Tree'),3000)

//Click on the taxonomy dropdown
WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Taxonomy_dropdown'))

WebUI.delay(2)

WebUI.waitForElementClickable(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Link'), 2000)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Link'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node Button'))

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Delete Node and SKUs link'))

WebUI.delay(2)

WebUI.click(findTestObject('AmazeTestObjects_UI/Taxonomy_UI/Delete Node/Confirm Deletion Button'))

WebUI.delay(15)

CustomKeywords.'com.amaze.utilities.commonUtilities.navigateToCatalogListingPage'()

println(((('Node ' + GlobalVariable.newNodeName) + ' from catalog ') + GlobalVariable.catalog) + ' has been deleted.')



